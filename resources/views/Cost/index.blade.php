@extends('layout.main')

@section('style')
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet"/>
@endsection

@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">খরচের তথ্যসমূহ</h1>

                    <a class="d-none d-sm-inline-block btn btn-primary bg-success shadow-sm" data-toggle="modal" data-target="#newCostAdd"> নতুন খরচ যোগ করুন </a>
                </div>
            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered cost-list" id="" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>নং</th>
                                    <th>খরচের ক্যাটাগরি</th>
                                    <th>তারিখ</th>
                                    <th>টাকার পরিমান</th>
                                    <th>বিস্তারিত</th>
                                    <th>অন্যান্য</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>নং</th>
                                    <th>খরচের ক্যাটাগরি</th>
                                    <th>তারিখ</th>
                                    <th>টাকার পরিমান</th>
                                    <th>বিস্তারিত</th>
                                    <th>অন্যান্য</th>
                                </tr>
                                </tfoot>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->
            <!--  new cost list -->
            <div class="modal fade" id="newCostAdd" tabindex="-1" role="dialog" aria-labelledby="newCostAdd" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="newCostAdd">নতুন খরচ যোগ করুন</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="save_cost" method="post">
                                @csrf
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="cost_category_id">খরচের ক্যাটাগরি * </label><br />
                                        <select class="custom-select col-md-12 select2Active" id="cost_category_id" name="cost_category_id" style="width: 100%;">
                                            <option value="">Choose Category...</option>
                                            @foreach($cost_category as $category)
                                                <option value="{{$category->id}}"> {{ $category->category_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="cost"> পরিমাণ টাকা * </label>
                                        <input type="text" class="form-control" id="cost" name="cost" placeholder="টাকা" />
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="cost_date">তারিখ * </label>
                                        <input type="date" class="form-control" id="cost_date" name="cost_date" placeholder="নাম" />
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <div class="form-group">
                                            <label for="cost_note">নোট  </label>
                                            <textarea class="form-control" id="cost_note" name="cost_note" placeholder="আপনার কাস্টমারে নোট  এখানে লিখুন" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn bg-danger" data-dismiss="modal">বাতিল করুন</button>
                                    <button type="submit" class="btn bg-success">পরিবর্তন গুলোর সংরক্ষন</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End employee model new salary -->

            <!--  customer Modal Edit -->
            <div class="modal fade" id="costUpdate" tabindex="-1" role="dialog" aria-labelledby="costUpdate" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="costUpdate">তথ্যগুলো পরিবর্তন করুন</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="update_cost">
                                @csrf
                                <input type="hidden" name="id" id="id">
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="update_cost_category_id">খরচের ক্যাটাগরি * </label><br />
                                        <select class="custom-select col-md-12 select2Active" id="update_cost_category_id" name="cost_category_id" style="width: 100%;">
                                            @foreach($cost_category as $category)
                                                <option value="{{$category->id}}"> {{ $category->category_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="update_cost_amount"> পরিমাণ টাকা * </label>
                                        <input type="text" class="form-control" id="update_cost_amount" name="cost" />
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="update_cost_date">তারিখ * </label>
                                        <input type="date" class="form-control" id="update_cost_date" name="cost_date"/>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <div class="form-group">
                                            <label for="update_cost_note">নোট  </label>
                                            <textarea class="form-control" id="update_cost_note" name="cost_note" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn bg-danger" data-dismiss="modal">বাতিল করুন</button>
                                    <button type="submit" class="btn bg-success">পরিবর্তন গুলোর সংরক্ষন</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End customer Modal Edit -->
            <!--  customer Modal Delete -->
            <div class="modal fade" id="customerModalDelete" tabindex="-1" role="dialog" aria-labelledby="customerModalDelete" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="customerModalDelete">তথ্যটি মুছ</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            আপনি কি এই তথ্যটি মুছতে চান?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bg-warning" data-dismiss="modal">না</button>
                            <button type="button" class="btn bg-info">হ্যাঁ</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end customer Modal Delete -->
        </div>
    </div>
@endsection
@section('modal')
    <div class="modal fade" id="updateSalary">
        <div class="modal-dialog modal-lg">
            <div id="salary"> </div>
        </div>
    </div>
@endsection


@section('script')

    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{asset('assets/js/select2.min.js')}}"></script>

    <script>
        $('.select2Active').select2();
    </script>
    <script>
        $(function () {

            var table = $('.cost-list').DataTable({
                processing: true,
                serverSide: true,
                order:[[0, 'asc']],
                ajax: "{{ route('cost.list') }}",
                columns: [
                    {data: 'rownum', name: 'rownum', orderable: false, searchable: false},
                    {data: 'category_name', name: 'cost_categories.category_name'},
                    {data: 'cost_date', name: 'cost_date'},
                    {data: 'cost', name: 'cost'},
                    {data: 'cost_note', name: 'cost_note'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

        });
    </script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#save_cost').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);

            $.ajax({
                type:'POST',
                url: "{{route('save.cost')}}",
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    if (response) {
                        //this.reset();
                        toastr.success(response.message);
                        $("#newCostAdd").modal('toggle');
                        $("#save_cost")[0].reset();
                        $('.cost-list').DataTable().ajax.reload();
                    }
                },
                error: function(response){
                    var addCostError = response.responseJSON;
                    var showErrorMessage = '';

                    $.each(addCostError.errors, function(key, value){
                        showErrorMessage =  value;
                        toastr.error(showErrorMessage);
                    });

                }
            });
        });
    </script>
    <script>
        function updateCost(id){
            $.get('cost/' + id, function (data){
                $("#id").val(data.id);
                $("#update_cost_category_id").val(data.cost_category_id);
                $("#update_cost_category_id").select2();
                $("#update_cost_amount").val(data.cost);
                $("#update_cost_date").val(data.cost_date);
                $("#update_cost_note").val(data.cost_note);
                $("#costUpdate").modal('toggle');

            });
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#update_cost').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);

            $.ajax({
                type:'POST',
                url: "{{route('update.cost')}}",
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    if (response) {
                        //this.reset();
                        toastr.success(response.message);
                        $("#costUpdate").modal('toggle');
                        $("#update_cost")[0].reset();
                        $('.cost-list').DataTable().ajax.reload();
                    }
                },
                error: function(response){
                    var costError = response.responseJSON;
                    var showErrorMessage = '';

                    $.each(costError.errors, function(key, value){
                        showErrorMessage =  value;
                        toastr.error(showErrorMessage);
                    });

                }
            });
        });
    </script>

    <script>
        function deleteCost(id) {
            if (confirm('Do you realy want to delete this record?')) {
                $.ajax({
                    url: 'delete-cost/' + id,
                    type: 'get',
                    data: {
                        _token: $("input[name=_token]").val()
                    },
                    success: function (response) {
                        toastr.success(response.message);
                        $('.cost-list').DataTable().ajax.reload();
                    },
                    error: function (err) {
                        toastr.error("Data can't be deleted !");

                    }
                });
            }
        }
    </script>
    <script>

        @if(Session::has('success'))
        toastr.success("{{Session::get('success')}}");
        @endif

        @if(count($errors) > 0)
        @foreach($errors->all() as $error)
        toastr.error("{{ $error }}");
        @endforeach
        @endif

    </script>
@endsection
