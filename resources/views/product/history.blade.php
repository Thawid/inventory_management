@extends('layout.main')
@section('style')
@endsection

@section('body')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 mt-lg-4 mt-4">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0"> প্রোডাক্ট হিসট্রি </h1>
                <a href="{{url('create-product')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="bi bi-people-fill"></i> নতুন প্রোডাক্ট </a>
            </div>
        </div>
        <!-- page header -->

        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered product-history" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>নং</th>
                                <th>স্টক ইন </th>
                                <th>স্টক আউট </th>
                                <th>স্টক  </th>
                                <th>তারিখ</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>নং</th>
                                <th>স্টক ইন </th>
                                <th>স্টক আউট </th>
                                <th>স্টক  </th>
                                <th>তারিখ</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @php $count = 1;
                                 $transection_amount = 0;
                            @endphp
                               @if(!empty($product_history))
                                   @foreach($product_history as $history)

                                        <tr>
                                            <td> {{ $loop->iteration  }}</td>
                                            <td> {{ bangla(optional($history)->product_qty) }}</td>
                                            <td> {{ bangla(optional($history)->stock_out) }}</td>
                                            <td>

                                                @if($history->product_qty)
                                                {{en2bnNumber($transection_amount += $history->product_qty)}}
                                                @else
                                                {{en2bnNumber($transection_amount -= $history->stock_out)}}
                                                @endif

                                            </td>
                                            <td> {{ bangla($history->created_at) }}</td>
                                        </tr>
                                   @endforeach
                               @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- end body content col-md-12 -->



    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>

<script>
    $(function () {

        var table = $('.product-history').DataTable({
            processing: true,
            serverSide: false,
        });

    });
</script>

@endsection
