@extends('layout.main')
@section('body')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">নতুন প্রোডাক্ট বা পণ্য যুক্ত করুন</h1>
                    <a href="{{url('products')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="bi bi-people-fill"></i> সকল প্রোডাক্ট এর তালিকা </a>
                </div>
            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <p>
                            <small><i>* দেওয়া প্রতিটি ঘরের তথ্য সঠিকভাবে প্রদান করতে হবে</i></small>
                        </p>
                        <form  action="{{route('store.product')}}" method="post">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="product_name">প্রোডাক্ট নাম * </label>
                                    <input type="text" class="form-control" id="product_name" name="product_name" value="{{old('product_name')}}" placeholder="প্রোডাক্ট" />
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="packing">পরিমাণ * </label>
                                    <input type="text" class="form-control" id="packing" name="packing" value="{{old('packing')}}" placeholder="৬০০" />
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="productSize"> সাইজ * </label>
                                    <select class="form-control" id="productSize" name="size">
                                        <option value="0">Select One</option>
                                        <option value="টি">টি</option>
                                        <option value="মি.লি">মি.লি</option>
                                        <option value="গ্রাম">গ্রাম</option>
                                        <option value="পিস">পিস</option>
                                        <option value="প্যাকেট">প্যাকেট</option>
                                        <option value="কেজি">কেজি </option>
                                        <option value="লিটার">লিটার</option>
                                        <option value="ব্যারেল"> ব্যারেল </option>
                                    </select>
                                </div>

                            </div>

                            <button type="submit" class="btn bg-success "> + প্রোডাক্ট যোগ করুন</button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->
        </div>
    </div>

@endsection

@section('script')

    <script>

        @if(Session::has('success'))
        toastr.success("{{Session::get('success')}}");
        @endif

        @if(count($errors) > 0)
        @foreach($errors->all() as $error)
        toastr.error("{{ $error }}");
        @endforeach
        @endif

    </script>
@endsection
