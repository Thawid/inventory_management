@extends('layout.main')
@section('style')
@endsection

@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">সকল প্রোডাক্ট এর তালিকা</h1>
                    <a href="{{url('create-product')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="bi bi-people-fill"></i> নতুন প্রোডাক্ট </a>
                </div>
            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered product-list" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>নং</th>
                                    <th>প্রোডাক্ট নাম</th>
                                    <th>লেভেল  </th>
                                    <th>বর্তমান স্ট্রোক কোয়ান্টিটি </th>
                                    <th>অন্যান্য</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>নং</th>
                                    <th>প্রোডাক্ট নাম</th>
                                    <th>লেভেল </th>
                                    <th>বর্তমান স্ট্রোক কোয়ান্টিটি</th>
                                    <th>অন্যান্য</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->
            <!--  customer Modal Product Add -->
            <div class="modal fade" id="productModalAdd" tabindex="-1" role="dialog" aria-labelledby="productModalAdd" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="productModalAdd">তথ্যগুলো পরিবর্তন করুন</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="add_product_qty">
                                <div class="form-row">
                                    <input type="hidden" id="product_id" name="id">
                                    <div class="form-group col-md-3">
                                        <label>প্রোডাক্ট নাম * </label>
                                        <p><input class="form-control" type="text" id="product-name" disabled></p>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>বর্তমান স্ট্রোক কোয়ান্টিটি* </label>
                                        <p><input class="form-control" type="text" id="product_current_qty" disabled></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="product_qty">নতুন স্ট্রোক কোয়ান্টিটি * </label>
                                        <input type="text" class="form-control" id="product_qty" name="product_qty" placeholder="স্ট্রোক কোয়ান্টিটি" />
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn bg-danger" data-dismiss="modal">বাতিল করুন</button>
                                    <button type="submit" class="btn bg-success">পরিবর্তন গুলোর সংরক্ষন</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End customer Modal roduct Add -->
            <!--  customer Modal Edit -->
            <div class="modal fade" id="productModalEdit" tabindex="-1" role="dialog" aria-labelledby="productModalEdit" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="productModalEdit">তথ্যগুলো পরিবর্তন করুন</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="update_product">
                                <input type="hidden" id="id" name="id">
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="product_name">প্রোডাক্ট নাম * </label>
                                        <input type="text" class="form-control" id="product_name" name="product_name"/>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="packing">পরিমাণ * </label>
                                        <input type="text" class="form-control" id="packing" name="packing"/>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="productSize"> সাইজ * </label>
                                        <select class="form-control" id="size" name="size">
                                            <option value="টি">টি</option>
                                            <option value="মি.লি">মি.লি</option>
                                            <option value="গ্রাম">গ্রাম</option>
                                            <option value="পিস">পিস</option>
                                            <option value="প্যাকেট">প্যাকেট</option>
                                            <option value="কেজি">কেজি </option>
                                            <option value="লিটার">লিটার</option>
                                            <option value="ব্যারেল">ব্যারেল</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn bg-danger" data-dismiss="modal">বাতিল করুন</button>
                                    <button type="submit" class="btn bg-success">পরিবর্তন গুলোর সংরক্ষন</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End customer Modal Edit -->
            <!--  customer Modal Delete -->
            <div class="modal fade" id="productModalDelete" tabindex="-1" role="dialog" aria-labelledby="productModalDelete" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="productModalDelete">তথ্যটি মুছ</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            আপনি কি এই তথ্যটি মুছতে চান?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bg-warning" data-dismiss="modal">না</button>
                            <button type="button" class="btn bg-info">হ্যাঁ</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end customer Modal Delete -->
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>

    <script>
        $(function () {

            var table = $('.product-list').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('product.list') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'product_name', name: 'product_name'},
                    {data: 'label', name: 'label'},
                    {data: 'product_qty', name: 'product_qty'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: 'packing', name: 'packing', visible: false, searchable:true},
                    {data: 'size', name: 'size',visible: false, searchable:true},
                ]
            });

        });
    </script>
    <script>

        function editProduct(id) {
            //console.log(id);
            $.get('product/' + id, function (product) {

                $("#id").val(id);
                $("#product_name").val(product.product_name);
                $("#packing").val(product.packing);
                $("#size").val(product.size);
                $("#productModalEdit").modal('toggle');
            });
        }

        function deleteProduct(id) {
            if (confirm('Do you realy want to delete this record?')) {
                $.ajax({
                    url: 'delete-product/' + id,
                    type: 'get',
                    data: {
                        _token: $("input[name=_token]").val()
                    },
                    success: function (response) {
                        toastr.success(response.message);
                        $('.product-list').DataTable().ajax.reload();
                    },
                    error: function (err) {
                        toastr.error("Data can't be deleted !");

                    }
                });
            }
        }
    </script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#update_product').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);

            $.ajax({
                type:'POST',
                url: "{{route('update.product')}}",
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    if (response) {
                        //this.reset();
                        toastr.success(response.message);
                        $("#productModalEdit").modal('toggle');
                        $("#update_product")[0].reset();
                        $('.product-list').DataTable().ajax.reload();
                    }
                },
                error: function(response){
                    var updateProductError = response.responseJSON;
                    var showErrorMessage = '';

                    $.each(updateProductError.errors, function(key, value){
                        showErrorMessage =  value;
                        toastr.error(showErrorMessage);
                    });
                }
            });
        });

    </script>
    <script>
        function addProductQty(id) {
            $.get('product/' + id, function (product) {

                $("#product_id").val(product.id);
                $("#product-name").val(product.product_name);
                $("#product_current_qty").val(product.product_qty);
                $("#productModalAdd").modal('toggle');
            });
        }
    </script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#add_product_qty').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);

            $.ajax({
                type:'POST',
                url: "{{route('add.product.qty')}}",
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    if (response) {
                        //this.reset();
                        toastr.success("Product qty add successfully");
                        $("#productModalAdd").modal('toggle');
                        $("#add_product_qty")[0].reset();
                        $('.product-list').DataTable().ajax.reload();
                    }
                },
                error: function(response){
                    var addProductQtyError = response.responseJSON;
                    var showErrorMessage = '';

                    $.each(addProductQtyError.errors, function(key, value){
                        showErrorMessage =  value;
                        toastr.error(showErrorMessage);
                    });
                }
            });
        });

    </script>

@endsection
