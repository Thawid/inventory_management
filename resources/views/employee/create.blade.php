@extends('layout.main')

@section('style')
@endsection

@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">নতুন কর্মচারী</h1>
                    <a href="{{url('employee-list')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="bi bi-people-fill"></i> সকল কর্মচারী তালিকা </a>
                </div>
            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <p>
                            <small><i>* দেওয়া প্রতিটি ঘরের তথ্য সঠিকভাবে প্রদান করতে হবে</i></small>
                        </p>
                        <form  action="{{ route('save.employee') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="employeeName">কর্মচারী নাম * </label>
                                    <input type="text" class="form-control" id="employeeName" name="employee_name" value="{{ old('employee_name') }}" placeholder="নাম" />
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="employeeFather">পিতার নাম * </label>
                                    <input type="text" class="form-control" id="employeeFather" name="father_name" value="{{ old('father_name') }}" placeholder="পিতার নাম" />
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="employeeMobile">মোবাইল নম্বর * </label>
                                    <input type="text" class="form-control" id="employeeMobile" name="phone" value="{{ old('phone') }}" placeholder="০১৬৭৬০০০০০০" />
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <div class="form-group">
                                        <label for="employeeAddress">ঠিকানা * </label>
                                        <textarea class="form-control" id="employeeAddress" name="address" placeholder="আপনার কাস্টমারে ঠিকানা এখানে লিখুন
    "  rows="3">{{ old('address') }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="form-group">
                                        <label for="employeeNote">নোট * </label>
                                        <textarea class="form-control" id="employeeNote" name="note" placeholder="আপনার কাস্টমারে নোট  এখানে লিখুন
    "  rows="3">{{ old('note') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <p>আপলোড ছবি *:</p>
                                    <input type="file" id="employeePhoto" name="profile_pic">
                                </div>
                                <div class="form-group col-md-6">
                                    <p>আপলোড NID *:</p>
                                    <input type="file" id="employeeNID" name="nid">
                                </div>
                            </div>

                            <button type="submit" class="btn bg-success"> + তথ্য যোগ করুন</button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->
        </div>
    </div>
@endsection


@section('script')
    <script>

        @if(Session::has('success'))
        toastr.success("{{Session::get('success')}}");
        @endif

        @if(count($errors) > 0)
        @foreach($errors->all() as $error)
        toastr.error("{{ $error }}");
        @endforeach
        @endif

    </script>
@endsection
