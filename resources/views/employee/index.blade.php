@extends('layout.main')

@section('style')
    <style>
        .previewupload{
            height: 150px;
            width: 150px;
        }
    </style>
@endsection


@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">সকল কাস্টমারের তালিকা</h1>
                    <a href="{{url('create-employee')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                            class="bi bi-people-fill"></i> নতুন কর্মচারী </a>
                </div>
            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered employee-list-table" id="dataTable" width="100%"
                                   cellspacing="0">
                                <thead>
                                <tr>
                                    <th>নং</th>
                                    <th>কর্মচারী নাম</th>
                                    <th>পিতার নাম</th>
                                    <th>মোবাইল নম্বর</th>
                                    <th>ঠিকানা</th>
                                    <th>নোট</th>
                                    <th>অন্যান্য</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>নং</th>
                                    <th>কর্মচারী নাম</th>
                                    <th>পিতার নাম</th>
                                    <th>মোবাইল নম্বর</th>
                                    <th>ঠিকানা</th>
                                    <th>নোট</th>
                                    <th>অন্যান্য</th>
                                </tr>
                                </tfoot>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->

            <!--  customer Modal View -->
            <div class="modal fade" id="employeeDetails" tabindex="-1" role="dialog"
                 aria-labelledby="employeeDetails" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="employeeDetails">তথ্যগুলো </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="employee_name">কর্মচারী নাম </label>
                                    <input type="text" class="form-control" id="employee_name" name="employee_name"
                                           readonly/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="father_name">পিতার নাম </label>
                                    <input type="text" class="form-control" id="father_name" name="father_name" readonly/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="phone">মোবাইল নম্বর  </label>
                                    <input type="text" class="form-control" id="phone" name="phone"
                                           readonly>
                                </div>

                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <div class="form-group">
                                        <label for="address">ঠিকানা  </label>
                                        <textarea class="form-control" id="address" name="address"
                                                  placeholder="আপনার কাস্টমারে ঠিকানা এখানে লিখুন" rows="3"
                                                  readonly></textarea>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="form-group">
                                        <label for="note"> নোট </label>
                                        <textarea class="form-control" id="note" name="note"
                                                  readonly rows="3"
                                        ></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <div class="form-group">
                                        <label for="profile_pic"> Profile Picture </label><br>
                                        <img src="" id="preview-profile-pic" class="previewupload">
                                    </div>

                                </div>
                                <div class="form-group col-md-6">
                                    <div class="form-group">
                                        <label for="nid"> NID Card </label><br>
                                        <img src="" id="preview-nid-pic" class="previewupload">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bg-danger" data-dismiss="modal">বাতিল করুন</button>

                        </div>
                    </div>
                </div>
            </div>
            <!-- End customer Modal View -->

            <!--  customer Modal Edit -->
            <div class="modal fade" id="updateEmployee" tabindex="-1" role="dialog"
                 aria-labelledby="updateEmployee" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="updateEmployee">তথ্যগুলো পরিবর্তন করুন</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="update_employee" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" id="empid">
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="employeeName">কর্মচারী নাম * </label>
                                        <input type="text" class="form-control" id="employeeName" name="employee_name"
                                               placeholder="মোহাম্মাদ মেহেদী ইসলাম"/>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="fatherName">পিতার নাম </label>
                                        <input type="text" class="form-control" id="fatherName" name="father_name"/>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="employeePhone">মোবাইল নম্বর * </label>
                                        <input type="text" class="form-control" id="employeePhone" name="phone"
                                               placeholder="০১৬৭৬৯৬৬২৬০"/>
                                    </div>

                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <div class="form-group">
                                            <label for="employeeAddress">ঠিকানা * </label>
                                            <textarea class="form-control" id="employeeAddress" name="address"
                                                      placeholder="আপনার কাস্টমারে ঠিকানা এখানে লিখুন" rows="3"
                                            ></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="form-group">
                                            <label for="employeeNote"> নোট </label>
                                            <textarea class="form-control" id="employeeNote" name="note"
                                                      placeholder="" rows="3"
                                            ></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <div class="form-group">
                                            <label for="profile_pic" class="btn bg-secondary"><i class="fa fa-upload" aria-hidden="true"></i>
                                                Upload Profile Picture </label>
                                            <input type="file" id="profile_pic" name="profile_pic" class="d-none">
                                            <br><img src="" id="preview_profile_pic" class="previewupload">
                                            <span class="text-danger" id="image-input-pic-error"></span>
                                        </div>

                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="form-group">
                                            <label for="nid" class="btn bg-secondary"><i class="fa fa-upload" aria-hidden="true"></i>
                                                Upload NID Card </label>
                                            <input type="file" id="nid" name="nid" class="d-none">
                                            <br><img src="" id="preview_nid_pic" class="previewupload">
                                            <span class="text-danger" id="image-input-nid-error"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn bg-danger" data-dismiss="modal">বাতিল করুন</button>
                                    <button type="submit" class="btn bg-success">পরিবর্তন গুলোর সংরক্ষন</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End customer Modal Edit -->
            <!--  customer Modal Delete -->

            <!-- end customer Modal Delete -->
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>

    <script>
        $(function () {

            var table = $('.employee-list-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('employee.list') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'employee_name', name: 'employee_name'},
                    {data: 'father_name', name: 'father_name'},
                    {data: 'phone', name: 'phone'},
                    {data: 'address', name: 'address'},
                    {data: 'note', name: 'note'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

        });
    </script>
    <script>
        function viewEmployee(id){
            $.get('employee/' + id, function (employee){

                var ppath = "{{ asset('storage/uploads/profile') }}";
                var npath = "{{ asset('storage/uploads/nid') }}";
                $("#employee_name").val(employee.employee_name);
                $("#father_name").val(employee.father_name);
                $("#phone").val(employee.phone);
                $("#address").val(employee.address);
                $("#note").val(employee.note);
                $("#preview-profile-pic").attr('src',ppath+'/'+employee.profile_pic);
                $("#preview-profile-pic").hide();
                $("#preview-nid-pic").attr('src',npath+'/'+employee.nid);
                $("#preview-nid-pic").hide();

                if(employee.profile_pic !== null){
                    $("#preview-profile-pic").show();
                }
                if(employee.nid !== null){
                    $("#preview-nid-pic").show();
                }
                $("#employeeDetails").modal('toggle');
            });
        }
    </script>

    <script>
        function editEmployee(id){
            $.get('employee/'+id, function (employee){
                var ppath = "{{ asset('storage/uploads/profile') }}";
                var npath = "{{ asset('storage/uploads/nid') }}";
                $("#empid").val(employee.id);
                $("#employeeName").val(employee.employee_name);
                $("#fatherName").val(employee.father_name);
                $("#employeePhone").val(employee.phone);
                $("#employeeAddress").val(employee.address);
                $("#employeeNote").val(employee.note);
                $("#preview_profile_pic").attr('src',ppath+'/'+employee.profile_pic);
                $("#preview_profile_pic").hide();
                $("#preview_nid_pic").attr('src',npath+'/'+employee.nid);
                $("#preview_nid_pic").hide();

                if(employee.profile_pic !== null){
                    $("#preview_profile_pic").show();
                }
                if(employee.nid !== null){
                    $("#preview_nid_pic").show();
                }
                $("#updateEmployee").modal('toggle');
            });
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#update_employee').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            $('#image-input-pic-error').text('');
            $('#image-input-nid-error').text('');

            $.ajax({
                type:'POST',
                url: "{{route('update.employee')}}",
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    if (response) {
                        //this.reset();
                        toastr.success(response.message);
                        $("#updateEmployee").modal('toggle');
                        $("#update_employee")[0].reset();
                        $('.employee-list-table').DataTable().ajax.reload();
                    }
                },
                error: function(response){
                    var updateEmployeeError = response.responseJSON;
                    var showErrorMessage = '';

                    $.each(updateEmployeeError.errors, function(key, value){
                        showErrorMessage =  value;
                        toastr.error(showErrorMessage);
                    });
                    $('#image-input-pic-error').text(response.responseJSON.errors.file);
                    $('#image-input-nid-error').text(response.responseJSON.errors.file);
                }
            });
        });
    </script>
    <script>
        function deleteEmployee(id) {
            if (confirm('Do you realy want to delete this record?')) {
                $.ajax({
                    url: 'delete-employee/' + id,
                    type: 'get',
                    data: {
                        _token: $("input[name=_token]").val()
                    },
                    success: function (response) {
                        toastr.success(response.message);
                        $('.employee-list-table').DataTable().ajax.reload();
                    },
                    error: function (err) {
                        toastr.error("Data can't be deleted !");

                    }
                });
            }
        }
    </script>

@endsection

