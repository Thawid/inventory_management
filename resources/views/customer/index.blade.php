@extends('layout.main')
@section('style')
    <style>
        .previewupload{
            height: 150px;
            width: 150px;
        }
    </style>
@endsection

@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">সকল কাস্টমারের তালিকা</h1>
                    <a href="{{url('customer-create')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                            class="bi bi-people-fill"></i> নতুন কাস্টমার </a>
                </div>
            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered customer-list-table" id="dataTable" width="100%"
                                   cellspacing="0">
                                <thead>
                                <tr>
                                    <th>নং</th>
                                    <th>কাস্টমার নাম</th>
                                    <th>প্রতিস্টানের নাম </th>
                                    <th>পিতার নাম</th>
                                    <th>মোবাইল নম্বর</th>
                                    <th>ঠিকানা</th>
                                    <th>অন্যান্য</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>নং</th>
                                    <th>কাস্টমার নাম</th>
                                    <th>প্রতিস্টানের নাম </th>
                                    <th>পিতার নাম</th>
                                    <th>মোবাইল নম্বর</th>
                                    <th>ঠিকানা</th>
                                    <th>অন্যান্য</th>
                                </tr>
                                </tfoot>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->

            <!--  customer Modal View -->
            <div class="modal fade" id="customerModalView" tabindex="-1" role="dialog"
                 aria-labelledby="customerModalView" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="customerModalView">তথ্যগুলো </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="customerName">কাস্টমার নাম * </label>
                                    <input type="text" class="form-control" id="customerName" name="customer_name"
                                           readonly/>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="fatherName">পিতার নাম </label>
                                    <input type="text" class="form-control" id="fatherName" name="father_name" readonly/>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="phoneNumber">মোবাইল নম্বর * </label>
                                    <input type="text" class="form-control" id="phoneNumber" name="phone"
                                           readonly>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="institute_name"> প্রতিস্টানের নাম   </label>
                                    <input type="text" class="form-control" name="institute_name" id="institute_name"/>
                                </div>

                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <div class="form-group">
                                        <label for="customerAddress">ঠিকানা * </label>
                                        <textarea class="form-control" id="customerAddress" name="address"
                                                  placeholder="আপনার কাস্টমারে ঠিকানা এখানে লিখুন" rows="3"
                                        readonly></textarea>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="form-group">
                                        <label for="customerNote"> নোট </label>
                                        <textarea class="form-control" id="customerNote" name="note"
                                                  readonly rows="3"
                                        ></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <div class="form-group">
                                        <label for="profile_pic"> Profile Picture </label><br>
                                          <img src="" id="preview-profile-pic" class="previewupload">
                                    </div>

                                </div>
                                <div class="form-group col-md-6">
                                    <div class="form-group">
                                        <label for="nid"> NID Card </label><br>
                                        <img src="" id="preview-nid-pic" class="previewupload">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bg-danger" data-dismiss="modal">বাতিল করুন</button>

                        </div>
                    </div>
                </div>
            </div>
            <!-- End customer Modal View -->

            <!--  customer Modal Edit -->
            <div class="modal fade" id="customerModalEdit" tabindex="-1" role="dialog"
                 aria-labelledby="customerModalEdit" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="customerModalEdit">তথ্যগুলো পরিবর্তন করুন</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="update_customer" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" id="id">
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="customer_name">কাস্টমার নাম * </label>
                                        <input type="text" class="form-control" id="customer_name" name="customer_name"
                                               placeholder="মোহাম্মাদ মেহেদী ইসলাম"/>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="father_name">পিতার নাম </label>
                                        <input type="text" class="form-control" id="father_name" name="father_name"/>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="phone">মোবাইল নম্বর * </label>
                                        <input type="text" class="form-control" id="phone" name="phone"
                                               placeholder="০১৬৭৬৯৬৬২৬০"/>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="institute_name"> প্রতিস্টানের নাম   </label>
                                        <input type="text" class="form-control" name="institute_name" id="update_institute_name"/>
                                    </div>

                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <div class="form-group">
                                            <label for="address">ঠিকানা * </label>
                                            <textarea class="form-control" id="address" name="address"
                                                      placeholder="আপনার কাস্টমারে ঠিকানা এখানে লিখুন" rows="3"
                                            ></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="form-group">
                                            <label for="note"> নোট </label>
                                            <textarea class="form-control" id="note" name="note"
                                                      placeholder="" rows="3"
                                            ></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <div class="form-group">
                                            <label for="profile_pic" class="btn bg-secondary"><i class="fa fa-upload" aria-hidden="true"></i>
                                                Upload Profile Picture </label>
                                            <input type="file" id="profile_pic" name="profile_pic" class="d-none">
                                            <br><img src="" id="preview_profile_pic" class="previewupload">
                                            <span class="text-danger" id="image-input-pic-error"></span>
                                        </div>

                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="form-group">
                                            <label for="nid" class="btn bg-secondary"><i class="fa fa-upload" aria-hidden="true"></i>
                                                Upload NID Card </label>
                                            <input type="file" id="nid" name="nid" class="d-none">
                                            <br><img src="" id="preview_nid_pic" class="previewupload">
                                            <span class="text-danger" id="image-input-nid-error"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn bg-danger" data-dismiss="modal">বাতিল করুন</button>
                                    <button type="submit" class="btn bg-success">পরিবর্তন গুলোর সংরক্ষন</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End customer Modal Edit -->
            <!--  customer Modal Delete -->

            <!-- end customer Modal Delete -->
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>

    <script>
        $(function () {

            var table = $('.customer-list-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('customer.list') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'customer_name', name: 'customer_name'},
                    {data: 'institute_name', name: 'institute_name'},
                    {data: 'father_name', name: 'father_name'},
                    {data: 'phone', name: 'phone'},
                    {data: 'address', name: 'address'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

        });
    </script>
    <script>
        function viewCustomer(id) {
            $.get('customer/' + id, function (customer) {

                var ppath = "{{ asset('storage/uploads/profile/') }}";
                var npath = "{{ asset('storage/uploads/nid/') }}";
                $("#customerName").val(customer.customer_name);
                $("#fatherName").val(customer.father_name);
                $("#phoneNumber").val(customer.phone);
                $("#customerAddress").val(customer.address);
                $("#customerNote").val(customer.note);
                $("#institute_name").val(customer.institute_name);
                $('#preview-profile-pic').attr('src', ppath+'/' + customer.profile_pic);
                $('#preview-profile-pic').hide();
                $('#preview-nid-pic').attr('src', npath+'/' + customer.nid);
                $('#preview-nid-pic').hide();
                if (customer.profile_pic !== null) {
                    $('#preview-profile-pic').show();
                }
                if(customer.nid !== null){
                    $('#preview-nid-pic').show();
                }
                $("#customerModalView").modal('toggle');
            });
        }

        function editCustomer(id) {
            $.get('customer/' + id, function (customer) {

                var ppath = "{{ asset('storage/uploads/profile/') }}";
                var npath = "{{ asset('storage/uploads/nid/') }}";
                //console.log(ppath);
                $("#id").val(customer.id);
                $("#customer_name").val(customer.customer_name);
                $("#father_name").val(customer.father_name);
                $("#phone").val(customer.phone);
                $("#address").val(customer.address);
                $("#note").val(customer.note);
                $("#update_institute_name").val(customer.institute_name);
                $('#preview_profile_pic').attr('src', ppath+'/' + customer.profile_pic);
                $('#preview_profile_pic').hide();
                $('#preview_nid_pic').attr('src', npath+'/' + customer.nid);
                $('#preview_nid_pic').hide();
                if (customer.profile_pic !== null) {
                    $('#preview_profile_pic').show();
                }
                if(customer.nid !== null){
                    $('#preview_nid_pic').show();
                }
                $("#customerModalEdit").modal('toggle');
            });
        }

        function deleteCustomer(id) {
            if (confirm('Do you realy want to delete this record?')) {
                $.ajax({
                    url: 'delete-customer/' + id,
                    type: 'get',
                    data: {
                        _token: $("input[name=_token]").val()
                    },
                    success: function (response) {
                        toastr.success(response.message);
                        $('.customer-list-table').DataTable().ajax.reload();
                    },
                    error: function (err) {
                        toastr.error("Data can't be deleted !");

                    }
                });
            }
        }
    </script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#update_customer').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            $('#image-input-pic-error').text('');
            $('#image-input-nid-error').text('');

            $.ajax({
                type:'POST',
                url: "{{route('update.customer')}}",
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    if (response) {
                        //this.reset();
                        toastr.success(response.message);
                        $("#customerModalEdit").modal('toggle');
                        $("#update_customer")[0].reset();
                        $('.customer-list-table').DataTable().ajax.reload();
                    }
                },
                error: function(response){
                    var customerUpdateError = response.responseJSON;
                    var showErrorMessage = '';

                    $.each(customerUpdateError.errors, function(key, value){
                        showErrorMessage =  value;
                        toastr.error(showErrorMessage);
                    });
                    $('#image-input-pic-error').text(response.responseJSON.errors.file);
                    $('#image-input-nid-error').text(response.responseJSON.errors.file);
                }
            });
        });

    </script>
@endsection
