@extends('layout.main')
@section('body')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">নতুন কাস্টমার</h1>
                    <a href="{{url('customers')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                            class="bi bi-people-fill"></i> সকল কাস্টমারের তালিকা </a>
                </div>
            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <p>
                            <small><i>* দেওয়া প্রতিটি ঘরের তথ্য সঠিকভাবে প্রদান করতে হবে</i></small>
                        </p>
                        <form action="{{route('save.customer')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="customer_name">কাস্টমার নাম * </label>
                                    <input type="text" class="form-control @error('customer_name') is-invalid @enderror"
                                           id="customer_name" name="customer_name" value="{{ old('customer_name') }}" placeholder="নাম"/>
                                    @error('customer_name')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="father_name">পিতার নাম * </label>
                                    <input type="text" class="form-control" name="father_name" id="father_name"
                                       value="{{ old('father_name') }}"    placeholder="পিতার নাম"/>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="phone">মোবাইল নম্বর * </label>
                                    <input type="text" class="form-control @error('phone') is-invalid @enderror"
                                         name="phone" value="{{ old('phone') }}"  id="phone" placeholder="০১৬৭৬০০০০০০"/>
                                    @error('phone')
                                    <span class="text-danger">{{{ $message }}}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="institute_name"> প্রতিস্টানের নাম   </label>
                                    <input type="text" class="form-control" name="institute_name" id="institute_name"
                                           value="{{ old('institute_name') }}"    placeholder="প্রতিস্টানের নাম"/>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <div class="form-group">
                                        <label for="address">ঠিকানা * </label>
                                        <textarea class="form-control" name="address" id="address" placeholder="আপনার কাস্টমারে ঠিকানা এখানে লিখুন
    " rows="3">{{ old('address') }}</textarea>
                                        @error('address')
                                        <span class="text-danger">{{{ $message }}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="form-group">
                                        <label for="note"> নোট * </label>
                                        <textarea class="form-control" name="note" id="note" placeholder="আপনার কাস্টমারে নোট  এখানে লিখুন
    " rows="3"> {{ old('note') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <p>আপলোড ছবি *:</p>
                                    <input type="file" id="profile_pic" name="profile_pic">

                                </div>
                                <div class="form-group col-md-6">
                                    <p> আপলোড NID *:</p>
                                    <input type="file" id="nid" name="nid">
                                </div>
                            </div>
                            <button type="submit" class="btn bg-success"> + তথ্য যোগ করুন</button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->
        </div>
    </div>

@endsection

@section('script')

    <script>

        @if(Session::has('success'))
        toastr.success("{{Session::get('success')}}");
        @endif

        @if(count($errors) > 0)
        @foreach($errors->all() as $error)
        toastr.error("{{ $error }}");
        @endforeach
        @endif

    </script>
@endsection
