 <div class="modal-content">
        <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">

            <table class="table">
                <thead>
                <tr>
                    <th>নং</th>
                    <th>পণ্যের নাম</th>
                    <th>লেভেল</th>
                    <th>পরিমাণ</th>

                </tr>
                </thead>
                <tbody>
                @php
                    $count = 1;
                @endphp
                @if(isset($sells_item))
                    @foreach($sells_item->invoice as $item)
                        <tr>
                            <td>{{ bangla($count++) }}</td>
                            <td> {{ $item->product->product_name }}</td>
                            <td> {{  bangla($item->product->packing .' '. $item->product->size) }}</td>
                            <td> {{ en2bnNumber($item->product_qty) }}</td>
                        </tr>
                    @endforeach
                @endif

                </tbody>
            </table>
            <div class="col-md-12">
                <div class="text-center">
                    @if(isset($sells_item))
                        <table class="table thtablefooter">
                            <tr>
                                <td></td>
                                <td> <strong>ডিজেল</strong> </td>
                                <td>: </td>
                                <td> <strong>{{en2bnNumber($sells_item->diesel_price)}}</strong> </td>
                                <td>টাকা</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td> <strong>মাছ বাজার</strong> </td>
                                <td>: </td>
                                <td> <strong>{{en2bnNumber($sells_item->fish_market_price)}}</strong></td>
                                <td>টাকা</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td> <strong>সর্বমোট</strong> </td>
                                <td>: </td>
                                <td> <strong>{{en2bnNumber($sells_item->total_cost)}}</strong></td>
                                <td>টাকা</td>
                            </tr>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>





