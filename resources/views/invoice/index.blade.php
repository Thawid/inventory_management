@extends('layout.main')

@section('style')
@endsection

@section('body')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">সবগুলো ইনভেস্ট</h1>
                    <a href="{{url('create-invoice')}}"
                       class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"> নতুন ইনভয়েস তৈরি করুন </a>
                </div>
            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered invoice-list" id="dataTable" width="100%"
                                   cellspacing="0">
                                <thead>
                                <tr>

                                    <th> ইনভয়েস নং  </th>
                                    <th>তারিখ</th>
                                    <th>ক্রেতার নাম</th>
                                    <th>পরিমাণ</th>
                                    <th>ডিজেল</th>
                                    <th>মুদি বাজার</th>
                                    <th>সর্বমোট</th>
                                    <th>অন্যান্য</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th> ইনভয়েস নং  </th>
                                    <th>তারিখ</th>
                                    <th>ক্রেতার নাম</th>
                                    <th>পরিমাণ</th>
                                    <th>ডিজেল</th>
                                    <th>মুদি বাজার</th>
                                    <th>সর্বমোট</th>
                                    <th>অন্যান্য</th>
                                </tr>
                                </tfoot>
                                <tbody>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->

        </div>
    </div>

@endsection

@section('modal')
    <div class="modal fade" id="invoiceDetails">
        <div class="modal-dialog modal-lg">
            <div id="invoice-data"> </div>
        </div>
    </div>

    <div class="modal fade" id="invoiceDelete" tabindex="-1" role="dialog" aria-labelledby="invoiceDelete" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="invoiceDelete">তথ্যটি মুছ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="delete-invoice">
                        @csrf
                        <input type="hidden" id="sell_id" name="sell_id">
                            আপনি কি এই তথ্যটি মুছতে চান?
                        <div class="modal-footer">
                            <button type="button" class="btn bg-warning" data-dismiss="modal">না</button>
                            <button type="submit" class="btn bg-info">হ্যাঁ</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>

    <script>
        $(function () {

            var table = $('.invoice-list').DataTable({
                order: [[2, "DESC"]],
                processing: true,
                serverSide: true,
                ajax: "{{ route('get.invoice.list') }}",
                columns: [

                    {data: 'invoice_no', name: 'sells.invoice_no'},
                    {data: 'created_at', name: 'sells.created_at'},
                    {data: 'customer_name', name: 'customers.customer_name'},
                    {data: 'total_qty', name: 'sells.total_qty'},
                    {data: 'diesel_price', name: 'sells.diesel_price'},
                    {data: 'fish_market_price', name: 'sells.fish_market_price'},
                    {data: 'total_cost', name: 'sells.total_cost'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

        });
    </script>


    <script>
        function viewInvoice(sell_id) {
            $.get('invoice-details/'+sell_id,function(data) {
                $("#invoiceDetails").modal('toggle');
                $('#invoice-data').html(data);
            });
        }
    </script>

    <script>
        function deleteInvoice(sell_id) {
            $.get('invoice-sell-id/'+sell_id,function(invoice) {
                $("#sell_id").val(invoice.sell_id);
                $("#invoiceDelete").modal('toggle');
            });
        }

        $("#delete-invoice").submit(function(e){

            e.preventDefault();

            //let sell_id = $("#sell_id").val();
            let sell_id = document.getElementById('sell_id').value;
            console.log(sell_id)
            let _token = $("input[name= _token]").val();
            $.ajax({
                url:'delete-invoice/'+sell_id,
                type:'get',
                data:{
                    sell_id:sell_id,
                    _token:$("input[name=_token]").val()
                },
                success:function(response){
                    toastr.success(response.message);
                    $("#invoiceDelete").modal('toggle');
                    $("#delete-invoice")[0].reset();
                    $('.invoice-list').DataTable().ajax.reload();
                },
                error:function (err){
                    toastr.error("Data can't be deleted !");

                }
            });

        });
    </script>


@endsection
