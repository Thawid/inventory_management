@extends('layout.main')

@section('style')
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet"/>
@endsection

@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">নতুন ইনভয়েস তৈরি করুন</h1>
                    <a href="{{url('invoice-list')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"> সকল
                        ইনভয়েস </a>
                </div>
            </div>
            <!-- page header -->

            <div class="col-md-6">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered product-list" id="dataTable" width="100%"
                                   cellspacing="0">
                                <thead>
                                <tr>
                                    <th>প্রোডাক্ট নাম</th>
                                    <th>লেভেল</th>
                                    <th>বর্তমান স্ট্রোক</th>
                                    <th>যোগ</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <form action="{{route('invoice.data')}}" onsubmit="return (validateForm());" method="post">
                    @csrf
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="mb-4">
                                <form>
                                    <div class="form-row align-items-center">
                                        <div class="col-md-12">
                                            <select class="custom-select bootstrap-select customer-list"
                                                    id="customer_id" name="customer_id">
                                                <option value="0" selected>Choose Customer...</option>
                                                @if(isset($customer_list))
                                                    @foreach($customer_list as $customer)
                                                        <option value="{{$customer->id}}">{{ $customer->customer_name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>

                                    </div>
                                </form>
                            </div>
                            <table class="table product_list">
                                <thead>
                                <tr>
                                    <th>পণ্যের নাম</th>
                                    <th>লেভেল</th>
                                    <th>বর্তমান স্ট্রোক</th>
                                    <th style="width: 200px">পরিমাণ</th>
                                    <th style="width: 15px"></th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                            <table class="table thtablefooter">
                                <tbody>
                                <tr>
                                    <td> ডিজেল  </td>
                                    <td>:</td>
                                    <td><input type="number" name="diesel_price" class="form-control" id="diesel_price"
                                               placeholder="১২৩৪৫" onblur="calculate();"/></td>
                                    <td>টাকা</td>
                                </tr>
                                <tr>
                                    <td> মুদি বাজার </td>
                                    <td>:</td>
                                    <td><input type="number" name="fish_market_price" class="form-control"
                                               id="fish_market_price"
                                               placeholder="১২৩৪৫ " onblur="calculate();"/></td>
                                    <td>টাকা</td>
                                </tr>
                                <tr>
                                    <td>সর্বমোট</td>
                                    <td>:</td>
                                    <td style="text-align: left;" class="total_cost"><span id="total_cost"></span></td>
                                    <td>টাকা</td>
                                    <input type="hidden" name="total_qty" id="total_qty">
                                    <input type="hidden" name="total_cost" id="total_product_price">

                                </tr>
                                </tbody>
                            </table>
                            <button type="submit" class="btn bg-success btn-block"><i class="far fa-bookmark"></i>
                                ইনভেস্টিং আপনি তৈরি করুন
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@section('script')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{asset('assets/js/select2.min.js')}}"></script>


    <script>
        $(function () {

            var table = $('.product-list').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('get.product.list') }}",
                columns: [
                    {data: 'product_name', name: 'product_name', className: 'product_name'},
                    {
                        data: 'label',
                        name: 'label',
                        searchable: false,
                        sortable: false,
                        visible: true,
                        className: 'product_size'
                    },
                    {data: 'product_qty', name: 'product_qty', className: 'stock_qty'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: 'packing', name: 'packing', searchable: true, sortable: true, visible: false},
                    {data: 'size', name: 'size', searchable: true, sortable: true, visible: false},
                ]
            });

        });
        $(document).ready(function () {
            $('.customer-list').select2();
        });
    </script>

    <script>

        var product_name = '';
        var product_size = '';
        var amount = '';


        function get_product_details(current) {

            product_name = $(current).parent().parent().find(".product_name").text();
            product_size = $(current).parent().parent().find(".product_size").text();
            stock_qty = $(current).parent().parent().find('.stock_qty').text();

        }

        function add_product_to_chart(product_id, context, product_id) {

            get_product_details(context);

            var row = '<tr><td class="single_name">' + product_name + '</td><td class="single_size">' + product_size + '</td>' +
                '<td class="stock_qty">' + stock_qty + '</td>\n' +
                '<input name="product_id[]" type="hidden" value="' + product_id + '" class="">\n' +
                '<td><input style="width:50px" name="product_qty[]" onkeyup="claculate_qty(this)" type="text" class="form-control single_qty qty-input product_qty"   required></td>\n' +
                '<td><a href="javascript:void(0);"><i onclick="back_product_to_list(' + product_id + ',this,' + product_id + ')" class="far fa-window-close"></i></a></td></tr>';
            $(".product_list").append(row);
            $(context).parent().parent().hide();
        }

        //return product to productlist from chart
        function back_product_to_list(product_id, context, product_id) {
            $(".product_" + product_id).show();
            $(context).parent().parent().parent().remove();

        }

        //calculate quantity and price of product
        function claculate_qty(context) {
            var qty = 0;
            qty = context.value;
            calculate_total();
        }

        function calculate() {
            let total_price = 0;
            let diseale_price = Number(document.getElementById('diesel_price').value);
            let fist_market_price = Number(document.getElementById('fish_market_price').value);
            total_price = diseale_price + fist_market_price;
            document.getElementById('total_cost').innerHTML = total_price;
            $("#total_product_price").val(total_price);

        }

        //calculate total sub of product
        function calculate_total() {
            var sum = 0;
            var total_qty = 0;
            var qty_chart = 0;
            $(".single_qty").each(function () {
                var val = $.trim($(this).val());
                qty_chart = $(this).parent().parent().find('.product_qty').val();
                total_qty = parseInt(qty_chart) + parseInt(total_qty);

            });
            $('#total_qty').val(total_qty);
        }

        function validateForm() {
            //e.preventDefault();
            var customer_error = "Customer not selected"
            var product_error = "Product not added to list"
            var customer_id = $("#customer_id").val();
            var error = false;
            if (customer_id == 0) {
                //$(".status").html(customer_error);
                alert(customer_error)
                return false;
            }

            if (!error) {
                return true;
            }
        }

    </script>

    <script>

        @if(Session::has('message'))
        toastr.success("{{Session::get('message')}}");
        @endif

        @if(count($errors) > 0)
        @foreach($errors->all() as $error)
        toastr.error("{{ $error }}");
        @endforeach
        @endif

    </script>
@endsection
