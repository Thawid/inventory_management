@extends('layout.main')

@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">ব্যাংক লেনদেন</h1>

                    <a class="d-none d-sm-inline-block btn btn-primary bg-success shadow-sm" data-toggle="modal" data-target="#banktransaction"> নতুন লেনদেন </a>
                </div>
            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered bank-transaction" id="" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>নং</th>
                                    <th>লেনদেন এর ধরন</th>
                                    <th>তারিখ</th>
                                    <th>টাকার পরিমান</th>
                                    <th>টাকার পরিমান</th>
                                    <th>বিস্তারিত</th>
                                    <th>অন্যান্য</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th colspan="3" class="text-right"></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->
            <!--  employee model new salary -->
            <div class="modal fade" id="banktransaction" tabindex="-1" role="dialog" aria-labelledby="banktransaction" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="banktransaction"> ব্যাংক লেনদেন </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="bank_transaction">
                                @csrf
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="transaction_type">লেনদেন এর ধরন * </label>
                                        <select class="custom-select " id="transaction_type" name="transaction_type">
                                            <option value="0" selected>Select Type</option>
                                            <option value="1">ডিপোজিট</option>
                                            <option value="2">উইথড্র</option>
                                            <option value="3">একাউন্ট ওপেনিং </option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="amount">টাকার পরিমান * </label>
                                        <input type="number" class="form-control" id="amount" name="amount" placeholder="টাকার" />
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="transaction_date"> তারিখ * </label>
                                        <input type="date" class="form-control" id="transaction_date" name="transaction_date" placeholder="বিস্তারিত" />
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="details">বিস্তারিত  </label>
                                        <textarea class="form-control" name="details" id="details" cols="30" rows="5"></textarea>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn bg-danger" data-dismiss="modal">বাতিল করুন</button>
                                    <button type="submit" class="btn bg-success">পরিবর্তন গুলোর সংরক্ষন</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End employee model new salary -->

            <!--  customer Modal Edit -->
            <div class="modal fade" id="UpdateBankTransaction" tabindex="-1" role="dialog" aria-labelledby="UpdateBankTransaction" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="UpdateBankTransaction">তথ্যগুলো পরিবর্তন করুন</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="update_bank_transaction">
                                @csrf
                                <input type="hidden" name="id" id="id">
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="update_transaction_type">লেনদেন এর ধরন * </label>
                                        <select class="custom-select " id="update_transaction_type" name="transaction_type">
                                            <option value="" selected>Select Type</option>
                                            <option value="1">ডিপোজিট</option>
                                            <option value="2">উইথড্র</option>
                                            <option value="3">একাউন্ট ওপেনিং </option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="update_amount">টাকার পরিমান * </label>
                                        <input type="number" class="form-control" id="update_amount" name="amount" placeholder="টাকার" />
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="update_transaction_date"> তারিখ * </label>
                                        <input type="date" class="form-control" id="update_transaction_date" name="transaction_date" placeholder="বিস্তারিত" />
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="update_details">বিস্তারিত  </label>
                                        <textarea class="form-control" name="details" id="update_details" cols="30" rows="5"></textarea>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn bg-danger" data-dismiss="modal">বাতিল করুন</button>
                                    <button type="submit" class="btn bg-success">পরিবর্তন গুলোর সংরক্ষন</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End customer Modal Edit -->
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            var table = $('.bank-transaction').DataTable({
                processing: true,
                serverSide: true,
                ajax:{
                    url: "{{ route('all.transaction') }}",
                },
                columns: [
                    {data: 'rownum', name: 'rownum',orderable: false, searchable: false},
                    {data: 'transaction_type', name: 'transaction_type'},
                    {data: 'transaction_date', name: 'transaction_date'},
                    {data: 'amount', name: 'amount'},
                    {data: 'deposit', name: 'deposit'},
                    {data: 'details', name: 'details'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
        });
    </script>

    <script>
        function editTransaction(id){
            $.get('get-transaction/'+id, function (transaction){

                $("#id").val(transaction.id);
                $("#update_transaction_type").val(transaction.transaction_type);
                $("#update_amount").val(transaction.amount);
                $("#update_transaction_date").val(transaction.transaction_date);
                $("#update_details").val(transaction.details);
                $("#UpdateBankTransaction").modal('toggle');
            });
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#bank_transaction').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            $.ajax({
                type:'POST',
                url: "{{route('store.transaction')}}",
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    if (response) {
                        //this.reset();
                        toastr.success(response.message);
                        $("#banktransaction").modal('toggle');
                        $("#bank_transaction")[0].reset();
                        $('.bank-transaction').DataTable().ajax.reload();
                    }
                },
                error: function(response){
                    var bankError = response.responseJSON;
                    var showErrorMessage = '';

                    $.each(bankError.errors, function(key, value){
                        showErrorMessage =  value;
                        toastr.error(showErrorMessage);
                    });
                }
            });
        });
    </script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#update_bank_transaction').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            $.ajax({
                type:'POST',
                url: "{{route('update.transaction')}}",
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    console.log(response);
                    if (response) {
                        //this.reset();
                        toastr.success(response.message);
                        $("#UpdateBankTransaction").modal('toggle');
                        $("#update_bank_transaction")[0].reset();
                        $('.bank-transaction').DataTable().ajax.reload();
                    }
                },
                error: function(response){
                    var transactionError = response.responseJSON;
                    var showErrorMessage = '';

                    $.each(transactionError.errors, function(key, value){
                        showErrorMessage =  value;
                        toastr.error(showErrorMessage);
                    });
                }
            });
        });
    </script>

    <script>
        function deleteTransaction(id) {
            if (confirm('Do you really want to delete this record?')) {
                $.ajax({
                    url: 'delete-transaction/' + id,
                    type: 'get',
                    data: {
                        _token: $("input[name=_token]").val()
                    },
                    success: function (response) {
                        toastr.success(response.message);
                        $('.bank-transaction').DataTable().ajax.reload();
                    },
                    error: function (err) {
                        toastr.error("Data can't be deleted !");

                    }
                });
            }
        }
    </script>


@endsection

