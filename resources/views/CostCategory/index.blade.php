@extends('layout.main')

@section('style')
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet"/>
@endsection

@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">ক্যাটাগরি তথ্য তালিকা</h1>

                    <a class="d-none d-sm-inline-block btn btn-primary bg-success shadow-sm" data-toggle="modal" data-target="#NewCostCategoryModal"> নতুন খরচের ক্যাটাগরি </a>
                </div>
            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered category-list" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>নং</th>
                                    <th>ক্যাটাগরি নাম</th>
                                    <th>বিস্তারিত</th>
                                    <th>অন্যান্য</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>নং</th>
                                    <th>ক্যাটাগরি নাম</th>
                                    <th>বিস্তারিত</th>
                                    <th>অন্যান্য</th>
                                </tr>
                                </tfoot>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->
            <!--  employee model new salary -->
            <div class="modal fade" id="NewCostCategoryModal" tabindex="-1" role="dialog" aria-labelledby="NewCostCategoryModal" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="NewCostCategoryModal">নতুন বেতন তথ্য</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="save_category" action="" method="post">
                                @csrf
                                <div class="form-row">

                                    <div class="form-group col-md-6">
                                        <label for="category_name"> ক্যাটাগরি নাম * </label>
                                        <input type="text" class="form-control" id="category_name" name="category_name" placeholder="নাম" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="category_description"> বিস্তারিত * </label>
                                        <input type="text" class="form-control" id="category_description" name="category_description" placeholder="বিস্তারিত" />
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn bg-danger" data-dismiss="modal">বাতিল করুন</button>
                                    <button type="submit" class="btn bg-success">পরিবর্তন গুলোর সংরক্ষন</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End employee model new salary -->

            <!--  customer Modal Edit -->
            <div class="modal fade" id="updateCategory" tabindex="-1" role="dialog" aria-labelledby="updateCategory" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="updateCategory">তথ্যগুলো পরিবর্তন করুন</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="update_category">
                                @csrf
                                <input type="hidden" id="id" name="id">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="category_name"> ক্যাটাগরি নাম * </label>
                                        <input type="text" class="form-control" id="categoryName" name="category_name" placeholder="নাম" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="category_description"> বিস্তারিত * </label>
                                        <input type="text" class="form-control" id="categoryDescription" name="category_description" placeholder="বিস্তারিত" />
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn bg-danger" data-dismiss="modal">বাতিল করুন</button>
                                    <button type="submit" class="btn bg-success">পরিবর্তন গুলোর সংরক্ষন</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End customer Modal Edit -->
            <!--  customer Modal Delete -->
            <div class="modal fade" id="customerModalDelete" tabindex="-1" role="dialog" aria-labelledby="customerModalDelete" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="customerModalDelete">তথ্যটি মুছ</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            আপনি কি এই তথ্যটি মুছতে চান?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bg-warning" data-dismiss="modal">না</button>
                            <button type="button" class="btn bg-info">হ্যাঁ</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end customer Modal Delete -->
        </div>
    </div>
@endsection
@section('modal')
    <div class="modal fade" id="updateSalary">
        <div class="modal-dialog modal-lg">
            <div id="salary"> </div>
        </div>
    </div>
@endsection


@section('script')

    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{asset('assets/js/select2.min.js')}}"></script>

    <script>
        $('.select2').select2();
    </script>
    <script>
        $(function () {

            var table = $('.category-list').DataTable({
                processing: true,
                serverSide: true,
                order:[[0, 'asc']],
                ajax: "{{ route('category.list') }}",
                columns: [
                    {data: 'rownum', name: 'rownum', orderable: false, searchable: false},
                    {data: 'category_name', name: 'category_name'},
                    {data: 'category_description', name: 'category_description'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

        });
    </script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#save_category').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);

            $.ajax({
                type:'POST',
                url: "{{route('save.category')}}",
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    if (response) {
                        //this.reset();
                        toastr.success(response.message);
                        $("#NewCostCategoryModal").modal('toggle');
                        $("#save_category")[0].reset();
                        $('.category-list').DataTable().ajax.reload();
                    }
                },
                error: function(response){
                    var costCategoryError = response.responseJSON;
                    var showErrorMessage = '';

                    $.each(costCategoryError.errors, function(key, value){
                        showErrorMessage =  value;
                        toastr.error(showErrorMessage);
                    });
                }
            });
        });
    </script>
    <script>
        function updateCaetgory(id){
            $.get('category/' + id, function (category){

                $("#id").val(category.id);
                $("#categoryName").val(category.category_name);
                $("#categoryDescription").val(category.category_description);
                $("#updateCategory").modal('toggle');
            });
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#update_category').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);

            $.ajax({
                type:'POST',
                url: "{{route('update.category')}}",
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    if (response) {
                        //this.reset();
                        toastr.success(response.message);
                        $("#updateCategory").modal('toggle');
                        $("#update_category")[0].reset();
                        $('.category-list').DataTable().ajax.reload();
                    }
                },
                error: function(response){
                    var updateCostCategoryError = response.responseJSON;
                    var showErrorMessage = '';

                    $.each(updateCostCategoryError.errors, function(key, value){
                        showErrorMessage =  value;
                        toastr.error(showErrorMessage);
                    });

                }
            });
        });
    </script>

    <script>
        function deleteCategory(id) {
            if (confirm('Do you realy want to delete this record?')) {
                $.ajax({
                    url: 'delete-category/' + id,
                    type: 'get',
                    data: {
                        _token: $("input[name=_token]").val()
                    },
                    success: function (response) {
                        toastr.success(response.message);
                        $('.category-list').DataTable().ajax.reload();
                    },
                    error: function (err) {
                        toastr.error("Data can't be deleted !");

                    }
                });
            }
        }
    </script>

@endsection
