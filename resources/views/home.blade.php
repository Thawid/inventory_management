@extends('layout.main')

@section('body')
    <div class="container-fluid">
        <h1 class="mt-4">ড্যাশবোর্ড</h1>

        <div class="card mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-warning text-white mb-4 text-center">
                            <div class="card-body">
                                <h5>মোট বিক্রি</h5>
                                <h3> {{ en2bnNumber($total_sell->total_sell) }} টাকা </h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-primary text-white mb-4 text-center">
                            <div class="card-body">
                                <h5>মোট খরচ </h5>
                                <h3>{{en2bnNumber($total_cost->total_cost + $total_salary->total_salary)}} টাকা</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-success text-white mb-4 text-center">
                            <div class="card-body">
                                <h5>দোকানের ক্যাশ</h5>
                                <h3>{{en2bnNumber(($total_collect_bill->total_collection + $bank_withdraw->total_withdraw) -($total_cost->total_cost + $total_salary->total_salary + $bank_diposite->total_deposite))}} টাকা</h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-info text-white mb-4 text-center">
                            <div class="card-body">
                                <h5>ব্যাংক</h5>
                                <h3>{{en2bnNumber(($opening_money->amount + $bank_diposite->total_deposite) - $bank_withdraw->total_withdraw)}} টাকা</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-info text-white mb-4 text-center">
                            <div class="card-body">
                                <h5> মোট বকেয়া </h5>
                                <h3>{{en2bnNumber($total_sell->total_sell- ($total_collect_bill->total_collection + $discount->total_discount))}} টাকা</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
