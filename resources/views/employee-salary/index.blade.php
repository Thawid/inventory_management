@extends('layout.main')

@section('style')
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet"/>
@endsection

@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">সকল কর্মচারী বেতন তালিকা</h1>

                    <a class="d-none d-sm-inline-block btn btn-primary bg-success shadow-sm" data-toggle="modal" data-target="#employeeSalary"> কর্মচারীদের বেতন </a>
                </div>
            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered salary-list" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>নং</th>
                                    <th>কর্মচারী নাম</th>
                                    <th>বেতনের পরিমাণ</th>
                                    <th>তারিখ</th>
                                    <th>নোট</th>
                                    <th>অন্যান্য</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>নং</th>
                                    <th>কর্মচারী নাম</th>
                                    <th>বেতনের পরিমাণ</th>
                                    <th>তারিখ</th>
                                    <th>নোট</th>
                                    <th>অন্যান্য</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->
            <!--  employee model new salary -->
            <div class="modal fade" id="employeeSalary" tabindex="-1" role="dialog" aria-labelledby="employeeSalary" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="employeeSalary">নতুন বেতন তথ্য</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="save_salary">
                                @csrf
                                <div  class="form-row">
                                    <div class="form-group col-md-4">
                                        @include('partials.employee-dropdown')
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="salary"> বেতনের পরিমাণ * </label>
                                        <input type="text" class="form-control" id="salary" name="salary" placeholder="টাকা" />
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="date">তারিখ * </label>
                                        <input type="date" class="form-control" id="date" name="date" placeholder="নাম" />
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <div class="form-group">
                                            <label for="note">নোট * </label>
                                            <textarea class="form-control" id="note" name="note" placeholder="আপনার কাস্টমারে নোট  এখানে লিখুন" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn bg-danger" data-dismiss="modal">বাতিল করুন</button>
                                    <button type="submit" class="btn bg-success">পরিবর্তন গুলোর সংরক্ষন</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End employee model new salary -->

            <!--  customer Modal Edit -->
            <div class="modal fade" id="updateSalary" tabindex="-1" role="dialog" aria-labelledby="updateSalary" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="updateSalary">তথ্যগুলো পরিবর্তন করুন</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="update_employee_salary">
                                @csrf
                                <input type="hidden" name="id" id="id">
                                <div  class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="employee_id">কর্মচারী নাম * </label><br />
                                        <select class="custom-select select2" name="employee_id" id="update_employee_id" style="width: 100%;">
                                            @foreach($employee_list as $employee)
                                                <option value="{{ $employee->id }}"> {{ $employee->employee_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="salary"> বেতনের পরিমাণ * </label>
                                        <input type="text" class="form-control" id="update_salary" name="salary" placeholder="টাকা" />
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="date">তারিখ * </label>
                                        <input type="date" class="form-control" id="update_date" name="date" placeholder="নাম" />
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <div class="form-group">
                                            <label for="note">নোট * </label>
                                            <textarea class="form-control" id="update_note" name="note" placeholder="আপনার কাস্টমারে নোট  এখানে লিখুন" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn bg-danger" data-dismiss="modal">বাতিল করুন</button>
                                    <button type="submit" class="btn bg-success">পরিবর্তন গুলোর সংরক্ষন</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End customer Modal Edit -->

        </div>
    </div>
@endsection
@section('modal')
    <div class="modal fade" id="updateSalary">
        <div class="modal-dialog modal-lg">
            <div id="salary"> </div>
        </div>
    </div>
@endsection


@section('script')

    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{asset('assets/js/select2.min.js')}}"></script>

    <script>
        $('.select2').select2();
    </script>
    <script>
        $(function () {

            var table = $('.salary-list').DataTable({
                processing: true,
                serverSide: true,
                order:[0,"desc"],
                ajax: "{{ route('salary.list') }}",
                columns: [
                    {data: 'rownum', name: 'rownum', orderable: false, searchable: false},
                    {data: 'employee_name', name: 'employees.employee_name'},
                    {data: 'salary', name: 'employee_salaries.salary'},
                    {data: 'date', name: 'employee_salaries.date'},
                    {data: 'note', name: 'employee_salaries.note'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

        });
    </script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#save_salary').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);

            $.ajax({
                type:'POST',
                url: "{{route('save.salary')}}",
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    if (response) {
                        //this.reset();
                        toastr.success(response.message);
                        $("#employeeSalary").modal('toggle');
                        $("#save_salary")[0].reset();
                        $('.salary-list').DataTable().ajax.reload();
                    }
                },
                error: function(response){
                    var addSalaryError = response.responseJSON;
                    var showErrorMessage = '';

                    $.each(addSalaryError.errors, function(key, value){
                        showErrorMessage =  value;
                        toastr.error(showErrorMessage);
                    });

                }
            });
        });
    </script>
    <script>
       function editSalary(id){
            $.get('get-salary/'+id, function (data){

                $("#id").val(data.id);
                $("#update_employee_id").val(data.employee_id);
                $('#update_employee_id').select2()
                $("#update_salary").val(data.salary);
                $("#update_date").val(data.date);
                $("#update_note").val(data.note);
                $("#updateSalary").modal('toggle');
            });
        }
    </script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#update_employee_salary').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);

            $.ajax({
                type:'POST',
                url: "{{route('update.salary')}}",
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    if (response) {
                        //this.reset();
                        toastr.success(response.message);
                        $("#updateSalary").modal('toggle');
                        $("#update_employee_salary")[0].reset();
                        $('.salary-list').DataTable().ajax.reload();
                    }
                },
                error: function(response){
                    //console.log(response);
                    var updaetSalaryError = response.responseJSON;
                    var showErrorMessage = '';

                    $.each(updaetSalaryError.errors, function(key, value){
                        showErrorMessage =  value;
                        toastr.error(showErrorMessage);
                    });

                }
            });
        });
    </script>

    <script>
        function deleteEmployeeSalary(id) {
            if (confirm('Do you realy want to delete this record?')) {
                $.ajax({
                    url: 'delete-salary/' + id,
                    type: 'get',
                    data: {
                        _token: $("input[name=_token]").val()
                    },
                    success: function (response) {
                        toastr.success(response.message);
                        $('.salary-list').DataTable().ajax.reload();
                    },
                    error: function (err) {
                        toastr.error("Data can't be deleted !");

                    }
                });
            }
        }
    </script>
    <script>

        @if(Session::has('success'))
        toastr.success("{{Session::get('success')}}");
        @endif

        @if(count($errors) > 0)
        @foreach($errors->all() as $error)
        toastr.error("{{ $error }}");
        @endforeach
        @endif

    </script>
@endsection
