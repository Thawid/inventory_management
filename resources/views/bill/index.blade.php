@extends('layout.main')

@section('style')
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet"/>
@endsection


@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">কাস্টমারের বিলের তালিকা</h1>
                </div>
            </div>
            <!-- page header -->
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                         <div class="row">
                            <div class="form-group col-md-4">
                                <label for="productSize"> কাস্টমার নাম * </label>
                                <select class="form-control select2" name="customer_name" id="customer_id">
                                    <option value=""> Select Customer </option>
                                    @foreach($customrs as $customr )
                                        <option value="{{ $customr->id }}">{{$customr->customer_name}} </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="button-group col-md-4 mt-4">
                                <button type="button" name="filter" id="filter" class="btn btn-primary btn-sm">Filter</button>
                                <button type="button" name="reset" id="reset" class="btn btn-success btn-sm">Reset</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered bill-list" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>নং</th>
                                    <th>ইনভয়েস</th>
                                    <th>তারিখ</th>
                                    <th>মোট বিল</th>
                                    <th>বিল প্রদান</th>
                                    <th>বাকি আছে</th>
                                    <th>ডিসকাউন্ট </th>
                                    <th>অন্যান্য</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </tfoot>

                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->
            <!--  employee model new salary -->
            <div class="modal fade" id="NewBillPayModal" tabindex="-1" role="dialog" aria-labelledby="NewBillPayModal" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="NewBillPayModal">নতুন বিল প্রদান</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="bill_payment" method="post">
                                @csrf
                                <input type="hidden" name="id" id="sell_id">
                                <div class="form-row">

                                    <div class="form-group col-md-2">
                                        <label for="total_cost"> মোট বিল * </label>
                                        <input type="number" class="form-control" name="total_cost" id="total_cost" readonly>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="paid"> বিল প্রদান * </label>
                                        <input type="number" class="form-control" name="paid" id="paid" readonly>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="due"> বাকি আছে * </label>
                                        <input type="number" class="form-control" name="due" id="due" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="pay_bill"> এখন বিল প্রদান * </label>
                                        <input type="number" class="form-control" id="pay_bill" name="pay_bill" required/>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="discount"> ডিসকাউন্ট   </label>
                                        <input type="number" class="form-control" id="discount" name="discount" required/>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn bg-danger" data-dismiss="modal">বাতিল করুন</button>
                                    <button type="submit" class="btn bg-success">পরিবর্তন গুলোর সংরক্ষন</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End employee model new salary -->

        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{asset('assets/js/select2.min.js')}}"></script>

    <script>
        $('.select2').select2();
    </script>
    <script>
        $(document).ready(function(){

            fill_datatable();

            function fill_datatable(customer_name = '')
            {
                var table = $('.bill-list').DataTable({
                    "footerCallback": function ( row, data, start, end, display ) {
                        var api = this.api(), data;

                        // converting to interger to find total
                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '')*1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };

                        // computing column Total of the complete result


                        var totalBill = api
                            .column( 3 )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );

                        var totalPaid = api
                            .column( 4 )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );
                        var totalDiscount = api
                            .column( 6 )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );

                        var totalDue =  intVal(totalBill) - intVal(totalPaid);
                        // Update footer by showing the total with the reference of the column index
                        $( api.column( 2 ).footer() ).html('Total');
                        $( api.column( 3 ).footer() ).html((Math.round(totalBill * 100) / 100).toFixed(2));
                        $( api.column( 4 ).footer() ).html((Math.round(totalPaid * 100) / 100).toFixed(2));
                        $( api.column( 5 ).footer() ).html((Math.round(totalDue * 100) / 100).toFixed(2));
                        $( api.column( 6 ).footer() ).html((Math.round(totalDiscount * 100) / 100).toFixed(2));
                    },
                    processing: true,
                    serverSide: true,
                    ajax:{
                        url: "{{ route('all.bill') }}",
                        data:{customer_name:customer_name}
                    },
                    columns: [
                        {data: 'rownum', name: 'rownum',orderable: false, searchable: false},
                        {data: 'invoice_no', name: 'sells.invoice_no'},
                        {data: 'created_at', name: 'sells.created_at'},
                        {data: 'total_cost', name: 'sells.total_cost'},
                        {data: 'paid', name: 'sells.paid'},
                        {data: 'due', name: 'sells.due'},
                        {data: 'discount', name: 'sells.discount'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
            }

            $('#filter').click(function(){
                var customer_name = $('#customer_id').val();

                if(customer_name != '')
                {
                    $('.bill-list').DataTable().destroy();
                    fill_datatable(customer_name);
                }
                else
                {
                    alert('Select Customer Name');
                }
            });

            $('#reset').click(function(){
                $('#customer_id').val('');
                $('.bill-list').DataTable().destroy();
                fill_datatable();
            });

        });
    </script>

    <script>
        function billPay(id){
            $.get('get-due-bill/'+id, function (pay_bill){

                $("#sell_id").val(pay_bill.id);
                $("#total_cost").val(pay_bill.total_cost);
                $("#paid").val(pay_bill.paid);
                $("#due").val(pay_bill.due);
                $("#discount").val(pay_bill.discount);
                $("#NewBillPayModal").modal('toggle');
            });
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#bill_payment').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            $.ajax({
                type:'POST',
                url: "{{route('bill.pay')}}",
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    if (response) {
                        //this.reset();
                        toastr.success(response.message);
                        $("#NewBillPayModal").modal('toggle');
                        $("#bill_payment")[0].reset();
                        $('.bill-list').DataTable().ajax.reload();
                    }
                },
                error: function(response){
                    var billPayError = response.responseJSON;
                    var showErrorMessage = '';

                    $.each(billPayError.errors, function(key, value){
                        showErrorMessage =  value;
                        toastr.error(showErrorMessage);
                    });
                }
            });
        });
    </script>


@endsection

