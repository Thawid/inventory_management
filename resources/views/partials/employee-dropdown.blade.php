<label for="employee_id">কর্মচারী নাম * </label><br />
<select class="custom-select col-md-12 select2" name="employee_id" id="employee_id" style="width: 100%;">
    <option value="" selected>Choose...</option>
    @foreach($employee_list as $employee)
        <option value="{{ $employee->id }}"> {{ $employee->employee_name }}</option>
    @endforeach
</select>
