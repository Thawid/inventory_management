<?php

use App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes([
       'register' => false,
       'reset' => true,
       'verify' => true,
]);



Route::get('/home', [Controllers\HomeController::class, 'index'])->name('home');

Route::get('/customers',[Controllers\CustomerController::class,'index']);
Route::get('/customer-list',[Controllers\CustomerController::class,'getAllCustomer'])->name('customer.list');
Route::get('/customer-create',[Controllers\CustomerController::class,'create_customer']);
Route::post('/save-customer',[Controllers\CustomerController::class,'store'])->name('save.customer');
Route::get('/customer/{id}',[Controllers\CustomerController::class,'get_customer_by_id']);
Route::post('/update-customer',[Controllers\CustomerController::class,'update_customer'])->name('update.customer');
Route::get('/delete-customer/{id}',[Controllers\CustomerController::class,'delete_customer'])->name('delete.customer');

Route::get('/products',[Controllers\ProductController::class,'index']);
Route::get('/product-list',[Controllers\ProductController::class,'get_product_list'])->name('product.list');
Route::get('/create-product',[Controllers\ProductController::class,'create_product']);
Route::post('/store-product',[Controllers\ProductController::class,'store'])->name('store.product');
Route::get('/product/{id}',[Controllers\ProductController::class,'get_product_by_id']);
Route::post('/update-product',[Controllers\ProductController::class,'update_product'])->name('update.product');
Route::post('/add-qty',[Controllers\ProductController::class,'add_product_qty'])->name('add.product.qty');
Route::get('/delete-product/{id}',[Controllers\ProductController::class,'delete_product'])->name('delete.product');
Route::get('/product-history/{id}','App\Http\Controllers\ProductController@product_history');


Route::get('/create-invoice',[Controllers\InvoiceController::class,'index'])->name('create.invoice');
Route::post('/invoice-data',[Controllers\InvoiceController::class,'save_invoice_data'])->name('invoice.data');
Route::get('/get-product-list',[Controllers\InvoiceController::class,'get_product_list'])->name('get.product.list');
Route::get('/invoice-list',[Controllers\InvoiceController::class,'invoice_list']);
Route::get('/get-invoice-list',[Controllers\InvoiceController::class,'get_invoice_list'])->name('get.invoice.list');
Route::get('/invoice-details/{id}',[Controllers\InvoiceController::class,'invoice_view']);
Route::get('/delete-invoice/{id}',[Controllers\InvoiceController::class,'delete_invoice']);
Route::get('/invoice-sell-id/{id}',[Controllers\InvoiceController::class,'get_sell_id']);

/*Route::get('/invoice-pdf/{id}',[Controllers\InvoiceController::class,'invoice_pdf_view'])->name('pdf.view');
Route::get('/invoice-print/{id}',[Controllers\InvoiceController::class,'get_invoice_print'])->name('print.view');*/
Route::get('/update-invoice/{id}','App\Http\Controllers\InvoiceController@invoice_update_view');
Route::post('/update-invoice-data/{id}',[Controllers\InvoiceController::class,'save_update_data'])->name('update.invoice.data');


Route::get('/employee-list',[Controllers\EmployeeController::class,'index']);
Route::get('/get-employee-list',[Controllers\EmployeeController::class,'get_all_employee'])->name('employee.list');
Route::get('/create-employee',[Controllers\EmployeeController::class,'create_employee']);
Route::post('/save-employee',[Controllers\EmployeeController::class,'save_employee_data'])->name('save.employee');
Route::get('/employee/{id}',[Controllers\EmployeeController::class,'get_employee_by_id']);
Route::post('/update-employee',[Controllers\EmployeeController::class,'udpate_employee'])->name('update.employee');
Route::get('/delete-employee/{id}',[Controllers\EmployeeController::class,'delete_employee'])->name('delete.employee');

Route::get('/employee-salary',[Controllers\EmployeeSalaryController::class,'index']);
Route::post('/save-salary',[Controllers\EmployeeSalaryController::class,'save_salary'])->name('save.salary');
Route::get('/get-salary-list',[Controllers\EmployeeSalaryController::class,'salary_list'])->name('salary.list');
Route::get('/get-salary/{id}',[Controllers\EmployeeSalaryController::class,'get_salary_by_id']);
Route::post('/update-salary',[Controllers\EmployeeSalaryController::class,'update_emp_salary'])->name('update.salary');
Route::get('/delete-salary/{id}',[Controllers\EmployeeSalaryController::class,'delete_employee_salary']);

Route::get('/cost-category',[Controllers\CostCategoryController::class,'index']);
Route::post('/save-category',[Controllers\CostCategoryController::class,'save_category'])->name('save.category');
Route::get('/get-all-category',[Controllers\CostCategoryController::class,'get_all_category'])->name('category.list');
Route::get('/category/{id}',[Controllers\CostCategoryController::class,'get_category_by_id']);
Route::post('/update-category',[Controllers\CostCategoryController::class,'update_category'])->name('update.category');
Route::get('/delete-category/{id}',[Controllers\CostCategoryController::class,'delete_category']);


Route::get('/cost-list',[Controllers\CostController::class,'index']);
Route::post('/save-cost',[Controllers\CostController::class,'save_cost'])->name('save.cost');
Route::get('/get-cost-list',[Controllers\CostController::class,'get_all_cost'])->name('cost.list');
Route::get('/cost/{id}',[Controllers\CostController::class,'get_cost_by_id']);
Route::post('/update-cost',[Controllers\CostController::class,'update_cost'])->name('update.cost');
Route::get('/delete-cost/{id}',[Controllers\CostController::class,'delete_cost']);

Route::get('/bill-list',[Controllers\BillController::class,'index']);
Route::get('/get-all-bill',[Controllers\BillController::class,'get_all_bill'])->name('all.bill');
Route::get('/get-due-bill/{id}',[Controllers\BillController::class,'get_due_bill']);
Route::post('/pay-bill',[Controllers\BillController::class,'pay_bill'])->name('bill.pay');

Route::get('/bank',[Controllers\BankController::class,'index']);
Route::post('/store-data',[Controllers\BankController::class,'store'])->name('store.transaction');
Route::get('/all-transaction',[Controllers\BankController::class,'get_all_transaction'])->name('all.transaction');
Route::get('get-transaction/{id}',[Controllers\BankController::class,'get_transaction_by_id']);
Route::post('/update-transaction',[Controllers\BankController::class,'updte_transaction'])->name('update.transaction');
Route::get('/delete-transaction/{id}',[Controllers\BankController::class,'delete_transaction']);



