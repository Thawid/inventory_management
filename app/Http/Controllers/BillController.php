<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Sell;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class BillController extends Controller
{
    public function index(){
        $customrs = DB::table('customers')->select('id','customer_name')->get();
        return view('bill.index',compact('customrs'));
    }


    public function get_all_bill(Request $request){

        if ($request->ajax()) {
            DB::statement(DB::raw('set @rownum=0'));
            if(!empty($request->customer_name)){
                $data = DB::table('customers')
                    ->join('sells', 'sells.customer_id', '=', 'customers.id')
                    ->select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'customers.id', 'customers.customer_name', 'sells.id as sell_id', 'sells.invoice_no', 'sells.paid', 'sells.due', 'sells.total_cost', 'sells.created_at','sells.discount')
                    ->where('customers.id',$request->customer_name)
                    ->get();
            }else {
                $data = DB::table('customers')
                    ->join('sells', 'sells.customer_id', '=', 'customers.id')
                    ->select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'customers.id', 'customers.customer_name', 'sells.id as sell_id', 'sells.invoice_no', 'sells.paid', 'sells.due', 'sells.total_cost', 'sells.created_at','sells.discount')
                    ->get();
            }
            return datatables()->of($data)
                ->addColumn('rownum',function ($row){
                    return bangla($row->rownum);
                })
                ->addColumn('invoice_no',function ($row){
                    return bangla($row->invoice_no);
                })
                ->addColumn('created_at',function ($row){
                    return bangla($row->created_at);
                })

                ->addColumn('total_cost', function ($row) {
                    return bn_money($row->total_cost);
                })
                ->addColumn('paid', function ($row) {
                    return (bn_money($row->paid)) ?? '0';
                })

                ->addColumn('due', function ($row) {
                    return bn_money($row->total_cost - $row->paid);
                })
                ->addColumn('discount', function ($row) {
                    return (bn_money($row->discount)) ?? '0';
                })
                ->addColumn('action', function ($row) {
                    $actionBtn = '<div class="btn-group" role="group" aria-label="Basic example">
                                            <button type="button" class="btn bg-success btn-sm" onclick="billPay('.$row->sell_id.');"> + New Bill Pay</button>
                                   </div>';
                    return $actionBtn;
                })
                ->setRowClass(function ($row) {
                    return 'sell_' . $row->sell_id;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }


    /*
     * Get due bill by invoice
     * */


    public function get_due_bill(Request $request){
        //$data = [];
        $bill = Sell::where('id',$request->id)->firstOrfail();

        /*$data[]= [
          "total_cost"=>$bill->total_cost,
          "paid"=>$bill->paid,
          "due"=>$bill->total_cost - $bill->paid
        ];*/
        return response()->json($bill);
    }

    /*
     * Bill by sell id
     * */


    public function pay_bill(Request $request){
        $request->validate([
            'pay_bill' => 'required|numeric|min:1',
        ]);
        $total_cost = $request->input('total_cost');
        $pay_bill = $request->input('pay_bill');
        $paid = $request->input('paid');
        $due = $total_cost - ($pay_bill + $paid);
        $pay = Sell::findOrFail($request->id);
        $pay->paid = $paid + $pay_bill;
        $pay->due = $due;
        $pay->discount = $request->input('discount');
        $pay->save();
        return response()->json(["message"=>"Bill Pay Successfully"]);
    }
}
