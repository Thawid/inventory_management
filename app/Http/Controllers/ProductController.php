<?php

namespace App\Http\Controllers;


use App\Models\Product;
use App\Models\ProductsHistory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use DataTables;

class ProductController extends Controller
{


    /*
     * Product list view
     *
     * */

    public function index()
    {
        return view('product.index');
    }


    /*
     * Get product list
     *
     * */

    public function get_product_list(Request $request)
    {
        if ($request->ajax()) {
            $data = Product::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('label', function ($row) {
                    return bangla($row->packing . ' ' . $row->size);
                })
                ->addColumn('product_qty',function ($row){
                    return en2bnNumber($row->product_qty);
                })
                ->addColumn('action', function ($row) {
                    $actionBtn = '<div class="btn-group" role="group" aria-label="Basic example">
                                            <a class="btn bg-success btn-sm" href="javascript:void(0);" onclick="addProductQty(' . $row->id . ')">+ Proudct Add</a>
                                            <a class="btn bg-warning btn-sm" href="javascript:void(0);" onclick="editProduct(' . $row->id . ');">
                                                                 Edit</a>
                                            <a class="btn bg-danger btn-sm" href="javascript:void(0);" onclick="deleteProduct(' . $row->id . ');">
                                                                Delete</a>
                                            <a href="' . action('App\Http\Controllers\ProductController@product_history', ['id' => $row->id]) . '" class="btn bg-primary btn-sm">
                                                                History</a>
                                           </div>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }


    /*
     * Crete new product blae
     * */

    public function create_product()
    {
        return view('product.create');
    }

    /*
     * Store new product data
     * */


    public function store(Request $request)
    {
        //dd($request->all());
        $message = [
            'product_name.required' => 'Product name must be required',
            'packing.required' => 'Quantity must be required',
            'size.required' => 'Label must be required',
        ];
        $validator = Validator::make($request->all(), [
            'product_name' => 'required',
            'packing' => 'required',
            'size' => 'required'
        ], $message);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $product = new Product();
            $product->product_name = $request->input('product_name');
            $product->packing = $request->input('packing');
            $product->size = $request->input('size');
            $product->save();
            return redirect()->back()->with('success', 'Product add successfully');
        }

    }


    /*
    * Gte product by product id
    * */

    public function get_product_by_id($id)
    {
        $product = Product::find($id);
        return response()->json($product);
    }

    /*
     * Update product info
     *
     * */

    public function update_product(Request $request)
    {


        $validator = $request->validate([
            'product_name' => 'required',
            'packing' => 'required',
            'size' => 'required'
        ]);

        if ($validator) {
            $product = Product::find($request->id);
            $product->product_name = $request->input('product_name');
            $product->packing = $request->input('packing');
            $product->size = $request->input('size');
            $product->save();
            return response()->json(['message'=>'Product update successfully']);
        } else {
            return response()->json(['error'=>'Can not be update']);

        }

    }

    /*
* Add product qty
*
* */


    public function add_product_qty(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'product_qty' => 'required|gte:1|numeric',
        ]);
        $product = Product::find($request->id);
        DB::beginTransaction();
        try {
            $product->product_qty += $request->input('product_qty');
            $product->save();
            try {
                $history = new ProductsHistory();
                $history->product_id = $request->input('id');
                $history->product_qty = $request->input('product_qty');
                try {
                    $history->save();
                    DB::commit();
                    return response()->json(['message'=>'Product qty add successfully']);
                } catch (\Exception $e) {
                    DB::rollBack();
                    return response()->json(['error'=>"Don't added". $e->getMessage()]);
                }
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(['error'=>"Don't added". $e->getMessage()]);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error'=>"Don't added". $e->getMessage()]);
        }

    }

    /*
     * Delete product
     *
     * */
    public function delete_product($id){
        $product = Product::find($id);
        $product->delete();
        return response()->json(['message'=>'Product delete successfully']);
    }

    /*
     * Get product history
     * */

    public function product_history($id){
        $product_history = ProductsHistory::where('product_id',$id)->get();
        return view('product.history',compact('product_history'));
    }
}




