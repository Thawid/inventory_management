<?php

namespace App\Http\Controllers;

use App\Models\CostCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class CostCategoryController extends Controller
{
    /*
     * Cost category list
     * */


    public function index(){
        return view('CostCategory.index');
    }


    /*
     * Save cost category name
     *
     * */

    public function save_category(Request $request){
        //dd($request->all());
        $validated = $request->validate([
            'category_name' => 'required|unique:cost_categories'
        ]);

        if($validated){
            //CostCategory::insert($validated);
            $category = new CostCategory();
            $category->category_name = $request->input('category_name');
            $category->category_description = $request->input('category_description');
            $category->save();
            return response()->json(["message"=>"Cost category add successfully"]);
        }else{
            return response()->json(["error"=>"Cost category can not added"]);
        }
    }


    public function get_all_category(Request $request){

        if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0'));
            $category = CostCategory::latest()->get(['id','category_name','category_description',DB::raw('@rownum  := @rownum  + 1 AS rownum')]);
            return DataTables::of($category)

                ->addColumn('action', function($row){

                    $actionBtn = '<div class="btn-group" role="group" aria-label="Basic example">
                                            <a class="btn bg-warning btn-sm" href="javascript:void(0);" onclick="updateCaetgory('.$row->id.');">
                                                                 Edit</a>
                                            <a class="btn bg-danger btn-sm" href="javascript:void(0);" onclick="deleteCategory('.$row->id.');">
                                                                Delete</a></div>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->removeColumn('id')
                ->make(true);
        }
    }


    /*
     * Get cost category id category id
     *
     * */

    public function get_category_by_id($id){
        $category = CostCategory::where('id',$id)->firstOrfail();
        return response()->json($category);
    }

    public function update_category(Request $request){
        //dd($request->all());
         $request->validate([
            'category_name' => 'required'
        ]);
        $category = CostCategory::find($request->id);
        $category->category_name = $request->input('category_name');
        $category->category_description = $request->input('category_description');
        $category->save();
        return response()->json(["message"=>"Cost category add successfully"]);
    }

    public function delete_category($id){
        $data = CostCategory::find($id);
        $data->delete();
        return response()->json(['message'=>'Category delete successfully']);
    }
}
