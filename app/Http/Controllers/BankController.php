<?php

namespace App\Http\Controllers;

use App\Http\Requests\TransectionFormRequest;
use App\Models\Bank;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;

class BankController extends Controller
{
    public function index(){
        return view('bank.index');
    }


    public function store(TransectionFormRequest $request){
        //dd($request->all());
        $validated = $request->validated();
        if($validated){
            Bank::insert($validated);
            return response()->json(["message"=>"Transaction add successfully"]);
        }else{
            return response()->json(["error"=>"Transaction can not be added"]);
        }

    }



    public function get_all_transaction(Request $request){

        if ($request->ajax()) {
            DB::statement(DB::raw('set @rownum=0'));
            $data = DB::table('bank_transactions')
                ->select(DB::raw('@rownum  := @rownum  + 1 AS rownum'),'transaction_type','amount','transaction_date','details','id');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('transaction_type',function ($row){
                    return ($row->transaction_type == 2) ? 'উইথড্র' : 'ডিপোজিট';
                })
                ->addColumn('amount',function ($row){
                    return en2bnNumber($row->amount);
                })
                ->addColumn('deposit',function ($row){
                    if($row->transaction_type == 1 || $row->transaction_type == 3){
                        global $transection_amount;
                       $transection_amount += $row->amount;
                        return en2bnNumber($transection_amount);
                     }else {
                        global $transection_amount;
                        $transection_amount -= $row->amount;
                        return en2bnNumber($transection_amount);
                    }


                })
                ->addColumn('transaction_date',function ($row){
                    $date = date_create($row->transaction_date);
                    return date_format($date,'d-m-Y');
                })
                ->addColumn('action', function($row){
                    $actionBtn = '<div class="btn-group" role="group" aria-label="Basic example">
                                            <a class="btn bg-warning btn-sm" href="javascript:void(0);" onclick="editTransaction('.$row->id.');">
                                                                 Edit</a>
                                            <a class="btn bg-danger btn-sm" href="javascript:void(0);" onclick="deleteTransaction('.$row->id.');">
                                                                Delete</a>
                                           </div>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->removeColumn('id')
                ->make(true);
        }
    }


    public function get_transaction_by_id($id){
        $transaction = Bank::findOrFail($id);
        return response()->json($transaction);
    }


    public function updte_transaction(TransectionFormRequest $request){
        //dd($request->all());
        $validatedata  = $request->validated();

        if ($validatedata){
            $data = Bank::findOrFail($request->id);
            $data->transaction_type = $request->transaction_type;
            $data->amount= $request->amount;
            $data->transaction_date = $request->transaction_date;
            $data->details = $request->details;
            $data->save();
            return response()->json(["message"=>"Transaction update successfully"]);
        }else{
            return response()->json(["errors"=>"Transaction can not be updated"]);
        }
    }

    public function delete_transaction($id){

        $deleteData = Bank::findOrFail($id);
        $deleteData->delete();
        return response()->json(["message"=>"Transaction delete successfully"]);
    }
}
