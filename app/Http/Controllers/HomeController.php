<?php

namespace App\Http\Controllers;

use App\Models\Loginlog;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $total_sell = $this->total_sell();
        $total_cost = $this->total_cost();
        $total_salary = $this->total_salary();
        $total_collect_bill = $this->total_collect_bill();
        $bank_diposite = $this->bank_diposite();
        $bank_withdraw = $this->bank_withdraw();
        $opening_money = $this->openong_meny();
        $discount = $this->get_total_discount();
        return view('home',compact('total_sell','total_cost','total_salary','total_collect_bill','bank_diposite','bank_withdraw','opening_money','discount'));
    }

    /*
     * Return total sell
     *
     * */
    private function total_sell(){
        $total_sell = DB::table('sells')->select(DB::raw('SUM(total_cost) as total_sell'))->first();
        return $total_sell;
    }

    /*
     * Return total cost
     *
     * */

    private function total_cost(){

        $total_cost = DB::table('costs')->select(DB::raw('SUM(cost) as total_cost'))->first();
        return $total_cost;
    }

    /*
     * Return total salary paid
     *
     * */

    private function total_salary(){
        $total_salary = DB::table('employee_salaries')->select(DB::raw('SUM(salary) as total_salary'))->first();
        return $total_salary;
    }

    /*
     * Return total bill collection
     * */

    public function total_collect_bill(){
        $collect_bill = DB::table('sells')->select(DB::raw('SUM(paid) as total_collection'))->first();
        return $collect_bill;
    }


    /*
     * Get opening monet
     *
     * */

    public function openong_meny(){
        $opening_money = DB::table('bank_transactions')->select('amount')->where('transaction_type','=',3)->first();
        return $opening_money;
    }

    /*
     * Bank diposite
     * */

    public function bank_diposite(){
        $bank_diposite = DB::table('bank_transactions')->select(DB::raw('SUM(amount) as total_deposite'))
            ->where('transaction_type','=',1)
            ->first();
        return $bank_diposite;
    }

    /*
     * Bank withdraw
     * */

    public function bank_withdraw(){
        $bank_withdraw = DB::table('bank_transactions')->select(DB::raw('SUM(amount) as total_withdraw'))
            ->where('transaction_type','=',2)
            ->first();
        return $bank_withdraw;
    }

    /*
     * Get total Discount
     * */

    public function get_total_discount(){
        $discount = DB::table('sells')->select(DB::raw('SUM(discount) as total_discount'))->first();
        return $discount;
    }

    /*
     * Today total bill
     *
     * */

    private function today_bill(){
        $today_bill = DB::table('sells')
            ->select(DB::raw('SUM(total_product_price) as today_bill'))
            ->whereDate('created_at',Carbon::today())
            ->first();

        return $today_bill;
    }


    /*
     * Return last five login information
     *
     * */


    private function login_information(){
        $login_info = Loginlog::orderBy('id','DESC')->take(5)->get();
        return $login_info;
    }
}
