<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewCustomerPostRequst;
use App\Models\Customer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DataTables;

class CustomerController extends Controller
{



    /*
     * Customer list view
     *
     * */

    public function index(){
        return view('customer.index');
    }

    /*
     * Get all customer
     *
     * */

    public function getAllCustomer(Request $request){
        if ($request->ajax()) {
            $data = Customer::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<div class="btn-group" role="group" aria-label="Basic example">
                                            <a class="btn bg-success btn-sm" href="javascript:void(0);" onclick="viewCustomer('.$row->id.')">View</a>
                                            <a class="btn bg-warning btn-sm" href="javascript:void(0);" onclick="editCustomer('.$row->id.');">
                                                                 Edit</a>
                                            <a class="btn bg-danger btn-sm" href="javascript:void(0);" onclick="deleteCustomer('.$row->id.');">
                                                                Delete</a>
                                           </div>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }



    /*
     * Create new customer - blade file
     *
     * */

    public function create_customer()
    {
        return view('customer.create');
    }

    public function store(NewCustomerPostRequst $request)
    {

        $validated = $request->validated();
        //dd($request->all());

        $pic_name = null;
        $nid_pic_name = null;
        $customer = new Customer();
        if ($request->hasFile('profile_pic')) {
            $image = $request->file('profile_pic');
            $pic_name = time() . '.' . $image->extension();
            $request->file('profile_pic')->storeAs('uploads/profile', $pic_name, 'public');
            //$image->move(public_path('images/customer-profile-pic'), $pic_name);
        }

        if($request->hasFile('nid')){
            $nid_image = $request->file('nid');
            $nid_pic_name = time(). '.'.$nid_image->extension();
            $request->file('nid')->storeAs('uploads/nid', $nid_pic_name, 'public');
            //$nid_image->move(public_path('images/customer-nid'),$nid_pic_name);
        }

        $customer->customer_name = $request->input('customer_name');
        $customer->father_name = $request->input('father_name');
        $customer->phone = $request->input('phone');
        $customer->address = $request->input('address');
        $customer->note = $request->input('note');
        $customer->institute_name = $request->input('institute_name');
        $customer->profile_pic = $pic_name;
        $customer->nid = $nid_pic_name;
        $customer->save();
        return redirect()->back()->with('success','New customer add successfully');

    }


    /*
     * Find customer by id
     *
     * */
    public function get_customer_by_id($id){

        $customer = Customer::find($id);
        return response()->json($customer);
    }

    /*
     * Update customer information
     *
     * */
    public function update_customer(Request $request){

        //dd($request->profile_pic);

        $request->validate([
            'customer_name'=>'required',
            'phone'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:11|max:14',
            'address'=>'required',
            'profile_pic' => 'image|mimes:jpeg,png,jpg,svg',
            'nid' => 'image|mimes:jpeg,png,jpg,svg',
        ]);

        $customer = Customer::find($request->id);
        //$profile_img_name = null;
        if($request->file('profile_pic')){
            $nidPath = $request->file('profile_pic');
            $profile_img_name = time().'.'.$nidPath->getClientOriginalExtension();
            $request->file('profile_pic')->storeAs('uploads/profile', $profile_img_name, 'public');
            $customer->profile_pic = $profile_img_name;
        }
        if($request->file('nid')){
            $nidPath = $request->file('nid');
            $nid_img_name = time().'.'.$nidPath->getClientOriginalExtension();
            $request->file('nid')->storeAs('uploads/nid', $nid_img_name, 'public');
            $customer->nid = $nid_img_name;
        }

        $customer->customer_name = $request->customer_name;
        $customer->father_name = $request->father_name;
        $customer->phone = $request->phone;
        $customer->address = $request->address;
        $customer->note = $request->note;
        $customer->institute_name = $request->input('institute_name');

        $customer->save();

        $message = "Customer data update successfully";
        return response()->json(['message' => $message], 200);
    }

    /*
     * Delete customer method
     * */
    public function delete_customer($id){

        $data = Customer::find($id);
        $data->delete();
        return response()->json(['message'=>'Customer delete successfully']);
    }
}
