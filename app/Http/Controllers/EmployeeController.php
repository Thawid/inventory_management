<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeRequest;
use App\Models\Employee;
use Illuminate\Http\Request;
use DataTables;

class EmployeeController extends Controller
{
    /*
     * Employee list view
     * */

    public function index(){
        return view('employee.index');
    }

    /*
     * Get all employee data form database
     *
     * */


    public function get_all_employee(Request $request){

        if($request->ajax()){
            $data = Employee::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<div class="btn-group" role="group" aria-label="Basic example">
                                            <a class="btn bg-success btn-sm" href="javascript:void(0);" onclick="viewEmployee('.$row->id.')">View</a>
                                            <a class="btn bg-warning btn-sm" href="javascript:void(0);" onclick="editEmployee('.$row->id.');">
                                                                 Edit</a>
                                            <a class="btn bg-danger btn-sm" href="javascript:void(0);" onclick="deleteEmployee('.$row->id.');">
                                                                Delete</a>
                                           </div>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);

        }
    }

    /*
     * Add new employee blade file
     *
     * */


    public function create_employee(){
            return view('employee.create');
    }


    /*
     * Save employee data
     *
     * */

    public function save_employee_data(EmployeeRequest $request){
        //dd($request->all());
        $employee = new Employee();
        $employee_name = $request->input('employee_name');
        $father_name = $request->input('father_name');
        $phone = $request->input('phone');
        $address = $request->input('address');
        $note = $request->input('note');
        $pic_name = NULL;
        $nid_pic_name = NULL;
        if($request->hasFile('profile_pic')){
            $image = $request->file('profile_pic');
            $pic_name = time() . '.' . $image->extension();
            $request->file('profile_pic')->storeAs('uploads/profile', $pic_name, 'public');
        }

        if($request->hasFile('nid')){
            $nid_image = $request->file('nid');
            $nid_pic_name = time(). '.'.$nid_image->extension();
            $request->file('nid')->storeAs('uploads/nid', $nid_pic_name, 'public');
        }

        $employee->employee_name = $employee_name;
        $employee->father_name = $father_name;
        $employee->phone = $phone;
        $employee->address = $address;
        $employee->note = $note;
        $employee->profile_pic = $pic_name;
        $employee->nid = $nid_pic_name;
        $employee->save();

        return redirect()->back()->with('success','Employee create successfully');

    }

    /*
     * Get empoyee by id
     *
     * */

    public function get_employee_by_id($id){
        $employee = Employee::find($id);
        return response()->json($employee);
    }

    /*
     * Updaet employee data
     *
     * */


    public function udpate_employee(Request $request){

        //dd($request->all());
        $request->validate([
            'employee_name'=>'required',
            'father_name'=>'required',
            'phone'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:11|max:14',
            'profile_pic' => 'image|mimes:jpeg,png,jpg,svg',
            'nid' => 'image|mimes:jpeg,png,jpg,svg',
        ]);

        $employee = Employee::find($request->id);

        if($request->hasFile('profile_pic')){
            $image = $request->file('profile_pic');
            $pic_name = time() . '.' . $image->extension();
            $request->file('profile_pic')->storeAs('uploads/profile', $pic_name, 'public');
        }

        if($request->hasFile('nid')){
            $nid_image = $request->file('nid');
            $nid_pic_name = time(). '.'.$nid_image->extension();
            $request->file('nid')->storeAs('uploads/nid', $nid_pic_name, 'public');
        }

        $employee->employee_name = $request->input('employee_name');
        $employee->father_name = $request->input('father_name');
        $employee->phone = $request->input('phone');
        $employee->address = $request->input('address');
        $employee->note = $request->input('note');
        $employee->profile_pic = $pic_name;
        $employee->nid = $nid_pic_name;
        $employee->save();

        $message = "Employee data update successfully";
        return response()->json(['message' => $message], 200);
    }


    /*
     * Delete employee
     * */

    public function delete_employee($id){
        $data = Employee::find($id);
        $data->delete();
        return response()->json(['message'=>'Employee delete successfully']);
    }
}
