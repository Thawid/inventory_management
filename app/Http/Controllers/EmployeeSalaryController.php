<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\EmployeeSalary;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests\SalaryFormRequest;
use DataTables;
use Illuminate\Support\Facades\DB;

class EmployeeSalaryController extends Controller
{

    public function index(){
        $employee_list = Employee::select('id','employee_name')->get();
        return view('employee-salary.index',compact('employee_list'));
    }

    /*
     * Save salary data
     *
     * */


    public function save_salary(SalaryFormRequest $request){
        //dd($request->all());
        $validate = $request->validated();
        if($validate){
            $salary = new EmployeeSalary();
            $salary->employee_id = $request->input('employee_id');
            $salary->salary = $request->input('salary');
            $salary->date = $request->input('date');
            $salary->note = $request->input('note');
            $salary->save();
            return response()->json(["message"=>"Salary Added successfully"],200);
        }else{
            return response()->json(["error"=>"Salary can not be added"],400);
        }
    }


    /*
     * Employee salary list
     *
     * */

    public function salary_list(Request $request){
        if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0'));
            $data = DB::table('employee_salaries')
            ->join('employees','employees.id','=','employee_salaries.employee_id')
            ->select(DB::raw('@rownum  := @rownum  + 1 AS rownum'),'employees.employee_name','employee_salaries.id as salary_id','employee_salaries.salary','employee_salaries.date','employee_salaries.note')
            ;
            return DataTables::of($data)
                ->addColumn('salary',function ($row){
                    return en2bnNumber($row->salary);
                })
                ->addColumn('date',function ($row){
                    return bangla(date_format(date_create($row->date), 'd-m-Y'));
                })
                ->addColumn('action', function($row){

                    $actionBtn = '<div class="btn-group" role="group" aria-label="Basic example">
                                            <a class="btn bg-warning btn-sm" href="javascript:void(0);" onclick="editSalary('.$row->salary_id.');">
                                                                 Edit</a>
                                            <a class="btn bg-danger btn-sm" href="javascript:void(0);" onclick="deleteEmployeeSalary('.$row->salary_id.');">
                                                                Delete</a></div>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);

        }
    }


    /*
     * Get salary by id
     *
     * */

    public function get_salary_by_id($id){
        //$salary = EmployeeSalary::with('employee')->where('employee_salaries.id',$id)->first();
        $employee_list = Employee::select('id','employee_name')->get();
        $salary = DB::table('employee_salaries as empsalary')
            ->join('employees','employees.id','=','empsalary.employee_id')
            ->select('employees.id as employee_id','employees.id', 'employees.employee_name','empsalary.salary','empsalary.date','empsalary.note')
            ->where('empsalary.id',$id)
            ->first();
        return  response()->json($salary);
        //return view('employee-salary.update-salary',compact('salary','employee_list'));
    }


    public function update_emp_salary(SalaryFormRequest $request){
        $validate = $request->validated();
        if($validate){
            $salary = EmployeeSalary::findOrFail($request->id);
            $salary->employee_id = $request->input('employee_id');
            $salary->salary = $request->input('salary');
            $salary->date = $request->input('date');
            $salary->note = $request->input('note');
            $salary->save();
            return response()->json(["message"=>"Salary Update successfully"],200);
        }else{
            return response()->json(["error"=>"Salary can not be updated"],400);
        }
    }

    public function delete_employee_salary($id){
        $data = EmployeeSalary::find($id);
        $data->delete();
        return response()->json(['message'=>'Employee Salary delete successfully']);
    }
}
