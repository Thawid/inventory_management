<?php

namespace App\Http\Controllers;

use App\Models\Cost;
use App\Models\CostCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class CostController extends Controller
{
    public function index(){
        $cost_category = CostCategory::orderBy('id','DESC')->get();
        return view('Cost.index',compact('cost_category'));
    }


    public function save_cost(Request $request){
        $validated = $request->validate([
            'cost_category_id' => 'required',
            'cost' => 'required|integer',
            'cost_date'=>'required'
        ]);

        if($validated){
            $cost = new Cost();
            $cost->cost_category_id = $request->input('cost_category_id');
            $cost->cost = $request->input('cost');
            $cost->cost_date = $request->input('cost_date');
            $cost->cost_note = $request->input('cost_note');
            $cost->save();
            return response()->json(["message"=>"Cost add successfully"],200);
        }
    }


    public function get_all_cost(Request $request){
        if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0'));
            $cost = DB::table('costs')
                ->join('cost_categories','cost_categories.id','=','costs.cost_category_id')
                ->select('cost_categories.category_name','costs.id','costs.cost_date','costs.cost','costs.cost_note',DB::raw('@rownum  := @rownum  + 1 AS rownum'));
            return DataTables::of($cost)
                ->addColumn('cost_date',function ($row){
                    return bangla(date_format(date_create($row->cost_date), 'd-m-Y'));
                })
                ->addColumn('cost',function ($row){
                    return en2bnNumber($row->cost);
                })
                ->addColumn('action', function($row){

                    $actionBtn = '<div class="btn-group" role="group" aria-label="Basic example">
                                            <a class="btn bg-warning btn-sm" href="javascript:void(0);" onclick="updateCost('.$row->id.');">
                                                                 Edit</a>
                                            <a class="btn bg-danger btn-sm" href="javascript:void(0);" onclick="deleteCost('.$row->id.');">
                                                                Delete</a></div>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->removeColumn('id')
                ->make(true);
        }
    }


    public function get_cost_by_id($id){
        $cost = Cost::where('id',$id)->firstOrFail();
        return response()->json($cost);
    }


    public function update_cost(Request $request){
        $validated = $request->validate([
            'cost' => 'required|integer|min:1',
            'cost_date'=>'required'
        ]);

        if($validated){
            $cost = Cost::findOrFail($request->id);
            $cost->cost_category_id = $request->input('cost_category_id');
            $cost->cost = $request->input('cost');
            $cost->cost_date = $request->input('cost_date');
            $cost->cost_note = $request->input('cost_note');
            $cost->save();
            return response()->json(["message"=>"Cost update successfully"],200);
        }
    }

    public function delete_cost($id){
        $data = Cost::find($id);
        $data->delete();
        return response()->json(['message'=>'Category delete successfully']);
    }
}
