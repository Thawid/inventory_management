<?php

namespace App\Http\Controllers;

use App\Http\Requests\InvoiceFormRequest;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\Product;
use App\Models\ProductsHistory;
use App\Models\Sell;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Validation\Rules\In;
use Illuminate\View\View;

class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
     * Create new invoice view
     * */

    public function index(){
        $customer_list = Customer::select('id','customer_name')->get();
        return view('invoice.create',compact('customer_list'));
    }

    public function get_product_list(Request $request)
    {
        if ($request->ajax()) {
            $data = Product::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('label', function ($row) {
                    return bangla($row->packing . ' ' . $row->size);
                })
                ->addColumn('product_qty',function ($row){
                    return bangla($row->product_qty);
                })
                ->addColumn('action', function ($row) {
                    $actionBtn = '<a href="javascript:void(0);" onclick="add_product_to_chart(' . $row->id . ',this,' . $row->id . ')"><i class="fa fa-cart-plus" aria-hidden="true"></i></a>';
                    return $actionBtn;
                })
                ->setRowClass(function ($row) {
                    return "product_" . $row->id;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /*
     * Store invoice data
     * */

    public function save_invoice_data(InvoiceFormRequest $request){

        $request->validated();
        $customer_id = $request->input('customer_id');
        $product_id  = $request->input('product_id');
        $product_qty = $request->input('product_qty');
        $diesel_price = $request->input('diesel_price');
        $fist_market_price = $request->input('fish_market_price');
        $total_qty = $request->input('total_qty');
        $total_cost = $request->input('total_cost');
        $invoice_no = date("ym") . '-' . date("hi");
        $counter = 0;
        $product_history = [];
        DB::beginTransaction();
        try {
            $sell = new Sell();
            $sell->customer_id = $customer_id;
            $sell->invoice_no = $invoice_no;
            $sell->diesel_price = $diesel_price;
            $sell->fish_market_price = $fist_market_price;
            $sell->total_qty = $total_qty;
            $sell->total_cost = $total_cost;
            $sell->save();
            $sell_id = $sell->id;
            $insert_row = array();
            try {
                foreach ($product_id as $product) {
                    $insert_row[] = [
                        'product_id' => $product,
                        'product_qty' => $product_qty[$counter],
                        'customer_id' => $customer_id,
                        'sell_id' => $sell_id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),

                    ];
                    $counter++;
                }
                try {
                    Invoice::insert($insert_row);
                    try {
                        $count = 0;
                        foreach ($product_id as $product){
                            $qty = $product_qty[$count];
                            $products = Product::find($product);
                            $old_qty = $products->product_qty;
                            $new_qty = $old_qty - $qty;
                            DB::table('products')
                                ->where('id',$product)
                                ->update(['product_qty'=>$new_qty]);
                            $count++;
                        }
                        $i = 0;
                        foreach ($product_id as $product){
                            $product_history[]= [
                                'product_id'=>$product,
                                'stock_out'=>$product_qty[$i],
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                            ];
                            $i++;
                        }
                        try {
                            ProductsHistory::insert($product_history);
                            DB::commit();
                            return redirect()->back()->with('message', 'Invoice create successfully');
                        }catch (\Exception $e){
                            DB::rollBack();
                            return redirect()->back()->withErrors("Product history table error" . $e->getMessage());
                        }
                    }catch (\Exception $e){
                        DB::rollBack();
                        return redirect()->back()->withErrors("Product table error" . $e->getMessage());
                    }
                } catch (\Exception $e) {
                    DB::rollBack();
                    return redirect()->back()->withErrors("Invoice table error" . $e->getMessage());
                }
            } catch (\Exception $e) {
                DB::rollBack();
                return redirect()->back()->withErrors("Invoice table data error" . $e->getMessage());
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors("Seal table error" . $e->getMessage());
        }
    }


    /*
     * Invoice list view
     *
     * */

    public function invoice_list(){
        $sells_item = $this->invoice_view($sell_id = 0);
        return view('invoice.index');
    }

    /*
    * Get invoice list data
    *
    * */

    public function get_invoice_list(Request $request)
    {
        if ($request->ajax()) {
            DB::statement(DB::raw('set @rownum=0'));
            $data = DB::table('customers')
                ->join('sells', 'sells.customer_id', '=', 'customers.id')
                ->select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'customers.id', 'customers.customer_name', 'sells.id as sell_id', 'sells.invoice_no', 'sells.total_qty', 'sells.diesel_price','sells.fish_market_price', 'sells.total_cost', 'sells.created_at')
            ->orderBy('sell_id','DESC');
            //dd($data);
            return DataTables::of($data)
                ->addColumn('invoice_no',function ($row){
                    return bangla($row->invoice_no);
                })
                ->addColumn('created_at',function ($row){
                    $date = date_create($row->created_at);
                    return bangla(date_format($date,'d-m-Y'));
                })
                ->addColumn('total_qty', function ($row) {
                    return en2bnNumber($row->total_qty);
                })
                ->addColumn('diesel_price', function ($row) {
                    return en2bnNumber($row->diesel_price);
                })
                ->addColumn('fish_market_price', function ($row) {
                    return en2bnNumber($row->fish_market_price);
                })
                ->addColumn('total_cost', function ($row) {
                    return en2bnNumber($row->total_cost);
                })
                ->addColumn('action', function ($row) {
                    $actionBtn = '<div class="btn-group" role="group" aria-label="Basic example">
                                            <a class="btn bg-success btn-sm" href="javascript:void(0);" onclick="viewInvoice('.$row->sell_id.');"> View </a>
                                            <a href="' . action('App\Http\Controllers\InvoiceController@invoice_update_view', ['id' => $row->sell_id]) . '" type="button" class="btn bg-warning btn-sm">Edit</a>
                                            <a class="btn bg-danger btn-sm" href="javascript:void(0);" onclick="deleteInvoice('.$row->sell_id.');"> Delete </a>


                                        </div>';
                    return $actionBtn;
                })
                ->setRowClass(function ($row) {
                    return 'sell_' . $row->sell_id;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }


    public function invoice_view($sell_id)
    {
        $sells_item = Sell::with(['customer', 'invoice'])->where('id', $sell_id)->first();
        if ($sells_item) {
            return view('invoice.invoice-details',['sells_item'=>$sells_item]);

        } else {
            return redirect()->back()->withErrors('Sorry, Invoice not found');
        }
    }

    /*
    * View update information
    *
    * */
    public function invoice_update_view($sell_id)
    {
        $customer_list = Customer::select('id','customer_name')->get();
        $sells_item = Sell::with(['customer', 'invoice'])->where('id', $sell_id)->first();
        //dd($sells_item);
        if ($sells_item) {
            return view('invoice.update-invoice', ['sells_item' => $sells_item, 'customer_list' => $customer_list]);
        } else {
            return redirect()->back()->withErrors('Sorry, Invoice not found');
        }
    }

    /*
     * Delete product while update invoice
     *
     * */
    public function happy_remove_prodcut($sell_id, $product_id){

        $join_data  = DB::table('invoices')
            ->join('products','products.id','=','invoices.product_id')
            ->select('invoices.id','invoices.product_qty as inv_qty', 'products.product_qty as stk_qty')
            ->where('sell_id',$sell_id)
            ->where('product_id', $product_id)
            ->first();

        $new_qty = $join_data->stk_qty + $join_data->inv_qty;
        DB::table('products')
            ->where('id', $product_id)
            ->update(['product_qty'=> $new_qty]);

        DB::table('invoices')
            ->where('sell_id', $sell_id)
            ->where('product_id',$product_id)
            ->delete();
    }

    /*
     * Save update information
     *
     * */

    public function save_update_data(Request $request, $id)
    {

        //dd($request->all());

        $customer_id = $request->input('customer_id');
        $product_id  = $request->input('product_id');
        $product_qty = $request->input('product_qty');
        $rproduct_qty = $request->input('rproduct_qty');
        $diesel_price = $request->input('diesel_price');
        $fist_market_price = $request->input('fish_market_price');
        $total_qty = $request->input('total_qty');
        $total_cost = $request->input('total_cost');
        $sell_id = $request->input('sell_id');
        $rsell_id = $request->input('rsell_id');
        $counter = 0;
        $qty = [];
        $old_product_id = [];


        $count = 0;
        $formdata = [];
        $form_product_ids = [];
        DB::beginTransaction();

        try {
            $sell_data = Sell::find($id);
            //dd($sell_data);
            $sell_data->customer_id = $customer_id;
            $sell_data->diesel_price = $diesel_price;
            $sell_data->fish_market_price = $fist_market_price;
            $sell_data->total_qty = $total_qty;
            $sell_data->total_cost = $total_cost;
            $sell_data->save();

            foreach ($product_id as $product){
                $formdata[] =[
                    'product_id' => $product,
                    'product_qty' =>$product_qty[$count],
                    'old_qty' => (isset($rproduct_qty[$count])) ? $rproduct_qty[$count] : $product_qty[$count],
                    'rsell_id' => (isset($rsell_id[$count])) ? $rsell_id[$count] : 0,
                ];

                $form_product_ids[] = $product;

                $count++;
            }
            $old_qty = Invoice::where('sell_id','=',$sell_id)->get();
            foreach ($old_qty as $oldQty){
                $old_product_id[]= $oldQty->product_id;
            }

            foreach($formdata as $data){

                if($data['rsell_id'] == 0){
                    $insert_row = [
                        'product_id' => $data['product_id'],
                        'product_qty' => $data['product_qty'],
                        'customer_id' => $customer_id,
                        'sell_id' => $sell_id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];
                    Invoice::insert($insert_row);
                    $cq = Product::select('product_qty')->where('id',$data['product_id'])->first();
                    DB::table('products')
                        ->where('id',$data['product_id'])
                        ->update(['product_qty'=> $cq->product_qty - $data['product_qty']]);
                }

                if($data['rsell_id'] != 0){
                    $join_data  = DB::table('invoices')
                        ->join('products','products.id','=','invoices.product_id')
                        ->select('invoices.id','invoices.product_qty as inv_qty', 'products.product_qty as stk_qty')
                        ->where('sell_id',$sell_id)
                        ->where('product_id',$data['product_id'])
                        ->first();
                    //dd($join_data);
                    $db_qty = $data['old_qty'];
                    $stk_qty = $join_data->stk_qty;
                    $inv_id = $join_data->id;

                    if($db_qty > $data['product_qty']){
                        $update_qty = $db_qty - $data['product_qty'];
                        $new_qty = $stk_qty + $update_qty;
                    }elseif($db_qty < $data['product_qty']){
                        $update_qty = $data['product_qty'] - $db_qty;
                        $new_qty = $stk_qty - $update_qty;
                    }else{
                        $new_qty = $stk_qty;
                    }
                    DB::table('products')
                        ->where('id',$data['product_id'])
                        ->update(['product_qty'=>$new_qty]);

                    DB::table('invoices')
                        ->where('id', $inv_id)
                        ->where('product_id', $data['product_id'])
                        ->update(
                            [
                                'product_qty' => $data['product_qty'],
                                'customer_id'=>$customer_id
                            ]

                        );
                }

            }
            try {
                $difids = array_diff($old_product_id, $form_product_ids);
                if(count($difids) != 0){
                    foreach($difids as $dids){
                        $this->happy_remove_prodcut($sell_id, $dids);
                    }
                }
                DB::commit();
                return redirect()->back()->with('message', 'Invoice updated successfully');
            }catch (\Exception $e){
                DB::rollBack();
                return  redirect()->back()->withErrors('Invoice can not be updated'.$e->getMessage());
            }
        }catch (\Exception $e){
            DB::rollBack();
            return  redirect()->back()->withErrors('Invoice can not be updated'.$e->getMessage());
        }

    }

    /*
     * Delete invoice
     *
     * */
    public function get_sell_id($sell_id){
        $sell_id = Invoice::select('sell_id')->where('sell_id',$sell_id)->first();
        return response()->json($sell_id);
    }

    public function delete_invoice($sell_id){
        //dd($sell_id);
        DB::table('sells')->where('id', $sell_id)->delete();
        DB::table('invoices')->where('sell_id', $sell_id)->delete();
        return response()->json(['message'=>'Invoice delete successfully']);
    }
}
