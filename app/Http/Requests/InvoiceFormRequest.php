<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_id'=>'required',
            'customer_id.*'=>'required|exists:customers,id',
            'product_id'=>'required|array',
            'product_id.*'=>'required|exists:products,id',
            'product_qty'=>'required|array',
            'product_qty.*'=>'required|numeric|gte:1',
            'diesel_price'=>'required|numeric',
            'fish_market_price'=>'required|numeric',
            'total_qty'=>'required|numeric',
            'total_cost'=>'required',

        ];
    }


    public function messages()
    {
        return [
            'customer_id.required'=>'Customer name must be required',
            'customer_id.exists'=>'Customer not found',
            'product_id.array'=>'Invalid product list',
            'product_id.exists'=>'Product not found',
            'product_qty.array'=>'Invalid product quantity',
            'product_qty.*.gte'=>'Product quantity must be greater then or equal 1',
            'product_qty.numeric'=>'Product qty value must be numeric',
            'total_qty'=>'Total quantity must be required',
            'total_qty.numeric'=>'Invalid total qty',
            'total_cost.required'=>'Total price is required',
        ];
    }
}
