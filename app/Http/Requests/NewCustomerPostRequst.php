<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewCustomerPostRequst extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_name'=>'required|unique:customers',
            'phone'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:11|max:14|unique:customers',
            'address'=>'required',
            'profile_pic'=>'image|mimes:jpeg,png,jpg,gif,svg',
            'nid'=>'image|mimes:jpeg,png,jpg,gif,svg',
        ];

    }

    public function messages()
    {
        return [
          'customer_name.required'=>'Customer name must be required',
          'customer_name.unique' =>'The customer name has already exists',
          'address.required'=>'Customer address must be required',
          'phone.unique'=>'The phone number already taken',
        ];
    }
}
