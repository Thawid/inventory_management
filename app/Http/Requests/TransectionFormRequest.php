<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransectionFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "transaction_type" => "required",
            "amount" => "required|numeric|min:1",
            "transaction_date" => "required|date",
            "details" => "string|max:255"
        ];
    }


    public function messages()
    {
        return [
            'transaction_type.required'=>'Transaction type if required field',
            'amount.required'=>'Transaction field is required',
            'amount.regex'=>'Transaction amount must be positive number',
            'transaction_date.required'=>'Transaction date is required field',
            'details.max'=>'Maximum length is 255'
        ];
    }
}
