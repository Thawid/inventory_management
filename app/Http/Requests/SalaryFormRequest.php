<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SalaryFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id'=>'required',
            'salary'=>'required',
            'date'=>'required'
        ];
    }


    public function messages()
    {
        return [
            'employee_id.required'=>'Must be select employee',
            'salary.required'=>'Salary field must be required',
            'date'=>'Date must be required'
        ];
    }
}
