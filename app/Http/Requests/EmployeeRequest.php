<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_name'=>'required',
            'father_name'=>'required',
            'phone'=>'required|unique:employees',
            'address'=>'nullable',
            'note'=>'nullable',
            'profile_pic'=>'image|mimes:jpg,png,jpeg',
            'nid'=>'image|mimes:jpg,png,jpeg',
        ];
    }


    public function messages()
    {
        return [
            'employee_name.required'=>'Employee name must be required',
            'father_name.required'=>'Father name must be required',
            'phone.required'=>'Employee phone number must be required',
            'phone.unique'=>'Phone number already taken',
        ];
    }
}
