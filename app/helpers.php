<?php

function bangla($str) {
    $search=array("0","1","2","3","4","5",'6',"7","8","9");
    $replace=array("০",'১','২','৩','৪','৫','৬','৭','৮','৯');
    return str_replace($search,$replace,$str);
}

function en2bnNumber($amount, $dotted = false, bool $round = false)
{
    setlocale(LC_MONETARY, 'en_IN');
    if ($round) {

        //$number = money_format('%!i', round($amount));
        $number = round($amount);
    } else {
        //$number = money_format('%!i', $amount);
        $number = round($amount);
    }

    if (!$dotted) {
        $number = explode(".", $number);
        $number = $number[0];
    }
    $replace_array = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০", 'মধ্যাহ্ন', 'অপরাহ্ন','-');
    $search_array = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0", 'AM', 'PM','-');
    return str_replace($search_array, $replace_array, $number);
}

function bn_money($amount, $dotted = false, bool $round = false)
{
    setlocale(LC_MONETARY, 'en_IN');
    if ($round) {

        //$number = money_format('%!i', round($amount));
        $number = round($amount);
    } else {
        //$number = money_format('%!i', $amount);
        $number = round($amount);
    }

    if (!$dotted) {
        $number = explode(".", $number);
        $number = $number[0];
    }
    $replace_array = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0", 'AM', 'PM','-');
    $search_array = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0", 'AM', 'PM','-');
    return str_replace($search_array, $replace_array, $number);
}


function getBangladeshCurrencyInWord( $number)
{
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'One', 2 => 'Two',
        3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
        7 => 'Seven', 8 => 'Eight', 9 => 'Nine',
        10 => 'Ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
    $digits = array('', 'hundred','thousand','lakh', 'crore');
    while( $i < $digits_length ) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
        } else $str[] = null;
    }
    $Taka = implode('', array_reverse($str));
    $poysa = ($decimal) ? " and " . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' poysa' : '';
    return ($Taka ? $Taka . 'taka ' : '') . $poysa ;
}
