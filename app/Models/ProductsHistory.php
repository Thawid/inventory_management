<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductsHistory extends Model
{
    use HasFactory;

    protected $table = "products_history";

    protected $fillable = [
        "product_id",
        "product_qty"
    ];
}
