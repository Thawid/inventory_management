<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sell extends Model
{
    use HasFactory;

    protected $table = "sells";

    protected $fillable = [
        "customer_id",
        "invoice_no",
        "diesel_price",
        "fish_market_price",
        "total_qty",
        "total_cost",
        "paid",
        "due",
    ];

    /*
     * Every sell has one customer
     *
     * */

    public function customer(){
        return $this->hasOne('App\Models\Customer','id','customer_id')->select(['id','customer_name']);
    }

    /*
     * Every sell has may transaction
     *
     * */

    public function invoice(){
        return $this->hasMany('App\Models\Invoice','sell_id','id')->with(['product']);
    }
}
