<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $table = "employees";

    protected $fillable = [
        'employee_name',
        'father_name',
        'phone',
        'address',
        'note',
        'profile_pic',
        'nid'
    ];

    public function salary(){

        return $this->hasMany(EmployeeSalary::class,'employee_id','id');
    }

}
