<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CostCategory extends Model
{
    use HasFactory;

    protected $fillable = ['category_name','category_description'];

    public function cost(){
        return $this->hasMany(Cost::class,'cost_category_id','id');
    }
}
