<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Loginlog extends Model
{
    use HasFactory;

    protected $table = "loginlogs";

    protected $fillable = ['last_login_at','last_login_ip'];
}
