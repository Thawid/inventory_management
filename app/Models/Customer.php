<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $table = "customers";

    protected $fillable = [
        "customer_name",
        "father_name",
        "phone",
        "address",
        "note",
        "profile_pic",
        "nid",
        "institute_name"
    ];
}
