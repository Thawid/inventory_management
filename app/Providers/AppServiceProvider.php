<?php

namespace App\Providers;

use App\Models\Employee;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('partials.employee-dropdown',function ($view){
           $view->with('employee_list',Employee::select('id','employee_name')->get());
        });
    }
}
